const input = document.getElementById('file-input');
// console.log(input);
const image = document.getElementById('avatar-preview');

input.addEventListener('change', (e) => {
    if (e.target.files.length) {
        const src = URL.createObjectURL(e.target.files[0]);
        image.src = src;
    }
    // console.log(e);
});
