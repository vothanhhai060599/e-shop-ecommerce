<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::resource('user', AdUserController::class);

// Route::middleware(['memberNotLogin'])->group(function(){

//     /*----- Auth-----*/
//     Route::controller(Api\frontend\Authcontroller::class)->group(function(){
//         Route::get('login', 'showLogin')->name('login');
//         Route::post('login', 'checkLogin')->name('login');
//         Route::get('register', 'showRegister')->name('register');
//         Route::post('register', 'postRegister')->name('register');
//     });


// });

