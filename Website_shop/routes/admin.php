<?php


// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

/*----- Auth -----*/
Route::prefix('admin')->group(function(){
    Route::controller(auth\LoginController::class)->group(function(){
        Route::get('login', 'showLoginForm')->name('show.login.admin');
        Route::post('login', 'login');
    });


});

/*----- Admin -----*/
Route::prefix('admin')->middleware('admin')->group(function () {

    /*----- Dashboard -----*/
    Route::controller(admin\DashboardController::class)->group(function(){
        Route::get('/dashboard', 'index')->name('admin');
    });


    /*----- Profile -----*/
    $prefix = "profile";
    Route::prefix($prefix)->controller(admin\UserController::class)->group(function(){
        Route::get('/', 'showProfile')->name('profile');
        Route::post('/', 'updateProfile')->name('update_profile');
    });


    /*----- Country -----*/
    $prefix = "country";
    Route::prefix($prefix)->controller(admin\CountryController::class)->group(function(){
        Route::get('/', 'index')->name('country');
        Route::get('/add-country', 'showAddCountry')->name('show_country');
        Route::post('/add-country', 'addCountry')->name('add_country');
        Route::get('/update-country/{id_country}', 'showUpdateCountry')->name('show_update_country');
        Route::post('/update-country/{id_country}', 'updateCountry')->name('update_country');
        Route::get('/delete-country/{id_country}', 'deleteCountry')->name('delete_country');
    });


    /*----- Blog-----*/
    $prefix = "blog";
    Route::prefix($prefix)->controller(admin\BlogController::class)->group(function(){
        Route::get('/', 'showBlog')->name('show_blog');
        Route::get('/add-blog', 'showAddBlog')->name('show_add_blog');
        Route::post('/add-blog', 'addBlog')->name('add_blog');
        Route::get('/update-blog/{id_blog}', 'showUpdateBlog')->name('show_update_blog');
        Route::post('/update-blog/{id_blog}', 'updateBlog')->name('update_blog');
        Route::get('/delete-blog/{id_blog}', 'deleteBlog')->name('delete_blog');
    });


   /*----- Category -----*/
    Route::controller(admin\CategoryController::class)->group(function(){
        Route::get('category', 'category')->name('category');
        Route::post('add-category', 'addCate')->name('add.category');
        Route::get('add-category', 'showAddCate')->name('show.add.category');
    });

    /*----- Brand -----*/
    Route::controller(admin\BrandController::class)->group(function(){
        Route::get('brand', 'brand')->name('brand');
        Route::get('add-brand', 'showAddBrand')->name('show.add.brand');
        Route::post('add-brand', 'addBrand')->name('add.brand');
    });

    /*----- Hostiry order -----*/
    Route::controller(admin\CartController::class)->group(function(){
        Route::get('user-ordered', 'userOrdered')->name('user.ordered');
    });
});
?>
