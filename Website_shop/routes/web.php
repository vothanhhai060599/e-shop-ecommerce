<?php

// namespace App\Http\Controllers;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// Route::namespace('frontend')->group(function () {
//     Route::get('/', [HomeController::class, 'index'])->name('home_frontend');
// });

Route::prefix('')->group(function () {

    /*----- Home -----*/
    Route::controller(frontend\HomeController::class)->group(function(){
        Route::get('/', 'index')->name('home_frontend');
    });

     /*----- Authentication -----*/
    Route::controller(frontend\AuthController::class)->group(function(){
        Route::get('logout', 'logout')->name('logout');
    });

    /*----- Cart -----*/
    $prefix = "";
    Route::prefix($prefix)->controller(frontend\CartController::class)->group(function(){
        Route::get('cart-product', 'showCart')->name('show.cart');
        Route::post('register-order-cart', 'quickRegister')->name('register.order.cart');
        Route::post('cart-ajax', 'cart')->name('cart.ajax');
        Route::post('qty-up-ajax', 'cartQtyUp')->name('qty.up.ajax');
        Route::post('qty-down-ajax', 'cartQtyDown')->name('qty.down.ajax');
        Route::post('delete-cart-ajax', 'deleteCart')->name('delete.cart.ajax');
        Route::get('order-product', 'order')->name('order');
        Route::post('login-order-cart', 'login')->name('login.order.cart');
    });

    /*----- Blog -----*/
    $prefix = "";
    Route::prefix($prefix)->controller(frontend\BlogController::class)->group(function(){
        Route::get('blog', 'blog')->name('blog');
        Route::get('blog-single', 'blogSingle')->name('blogSingle');
        Route::get('blog-single/{id_blog}', 'showBlogSingle')->name('showBlogSingle');
        Route::post('blog-single-rate', 'rate')->name('rate');
        Route::get('blog-single-comment/{id_blog}', 'showComment')->name('showComment');
        Route::post('blog-single-comment/{id_blog}', 'comment')->name('comment');
    });

    /*----- Search -----*/
    $prefix = "";
    Route::prefix($prefix)->controller(frontend\SearchController::class)->group(function(){
        Route::get('search-advanced', 'searchAdvanced')->name('search.advanced');
        Route::post('search-advanced-ajax', 'searchAdvancedAjax')->name('search.advanced.ajax');
        Route::post('price-range-ajax', 'priceRange')->name('price.range.ajax');
    });

    /*----- Product -----*/
    $prefix = "";
    Route::prefix($prefix)->controller(frontend\ProductController::class)->group(function(){
        Route::get('product-detail/{id_product}', 'detail')->name('detail');
    });


    /*----- Check login -----*/
    Route::middleware(['memberNotLogin'])->group(function(){

        /*----- Auth-----*/
        Route::controller(frontend\Authcontroller::class)->group(function(){
            Route::get('login', 'showLogin')->name('login');
            Route::post('login', 'checkLogin')->name('login');
            Route::get('register', 'showRegister')->name('register');
            Route::post('register', 'postRegister')->name('register');
        });


    });

    /*----- check login -----*/
    $prefix = "account";
    Route::prefix($prefix)->group(function(){

        /*----- Auth-----*/
        Route::controller(frontend\AuthController::class)->group(function(){
            Route::get('user-update', 'showAccount')->name('show.account');
            Route::post('user-update','updateAccount')->name('update.account');
        });

        /*----- Product -----*/
        Route::controller(frontend\ProductController::class)->group(function(){
            Route::get('product', 'product')->name('product');
            Route::get('add-product', 'showAddProduct')->name('show.add.product');
            Route::post('add-product', 'addProduct')->name('add.product');
            Route::get('edit-product/{id}', 'showEditProduct')->name('show.edit.product');
            Route::post('edit-product/{id}', 'editProduct')->name('edit.product');
        });


    });
});


Auth::routes();

require("admin.php");
