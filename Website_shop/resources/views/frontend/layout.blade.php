<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | E-Shopper</title>
    <link href="{{ asset('frontend/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/prettyPhoto.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/price-range.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/rate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/avatar.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/multiple.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'> --}}
    <link rel="stylesheet" href="{{ asset('frontend/css/popup.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/span-select.css') }}">

    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
        href="{{ asset('frontend/images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
        href="{{ asset('frontend/images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
        href="{{ asset('frontend/images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed"
        href="{{ asset('frontend/images/ico/apple-touch-icon-57-precomposed.png') }}">
    <script src="{{ asset('frontend/js/jquery.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="https://kit.fontawesome.com/f9275dded9.js" crossorigin="anonymous"></script>




</head>
<!--/head-->

<body>


    @include('frontend.layout.header')

    @yield('content')

    @include('frontend.layout.footer')




    <script src="{{ asset('frontend/js/jquery.scrollUp.min.js') }}"></script>
    <script src="{{ asset('frontend/js/price-range.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery.prettyPhoto.js') }}"></script>
    <script src="{{ asset('frontend/js/main.js') }}"></script>
    <script src="{{ asset('frontend/js/avatar.js') }}"></script>
    <script src="{{ asset('frontend/js/product-detail.js') }}"></script>
    <script src="{{ asset('frontend/js/product.js') }}"></script>
    <script src="{{ asset('frontend/js/popup.js') }}"></script>
    <script src="{{ asset('frontend/js/signup.js') }}"></script>
    <script src="{{ asset('frontend/js/span-select.js') }}"></script>
    {{-- <script src="{{ asset("frontend/js/multipleimage.js")}}"></script> --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>

    <script>
        $(function() {
            $(".add-to-cart").click(function() {
                var is_login = $('.login').val();
                var id_product = $(this).data('id');
                var cart_product_price = $(".cart_product_price_" + id_product).val();
                var cart_product_qty = $(".cart_product_qty_" + id_product).val();
                var cart_product_name = $(".cart_product_name_" + id_product).val();
                var cart_product_image = $(".cart_product_image_" + id_product).val();

                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ url('cart-ajax') }}",
                    data: {
                        'cart_id': id_product,
                        'cart_name': cart_product_name,
                        'cart_price': cart_product_price,
                        'cart_image': cart_product_image,
                        'cart_qty': cart_product_qty,
                    },
                    success: function(data) {
                        var totalCart = 0;
                        // console.log(data['data']);
                        $.each(data['data'], function(key, val) {
                            totalCart = totalCart + parseInt(val['cart_qty']);
                        })
                        // console.log(totalCart);
                        $(".cart").html(totalCart + " <i class='fa fa-shopping-cart'></i>cart");
                    }
                })





            })
        })
    </script>

</body>

</html>
