@extends('frontend.layout')
@section('content')
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Sportswear
                                    </a>
                                </h4>
                            </div>
                            <div id="sportswear" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="">Nike </a></li>
                                        <li><a href="">Under Armour </a></li>
                                        <li><a href="">Adidas </a></li>
                                        <li><a href="">Puma</a></li>
                                        <li><a href="">ASICS </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#mens">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Mens
                                    </a>
                                </h4>
                            </div>
                            <div id="mens" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="">Fendi</a></li>
                                        <li><a href="">Guess</a></li>
                                        <li><a href="">Valentino</a></li>
                                        <li><a href="">Dior</a></li>
                                        <li><a href="">Versace</a></li>
                                        <li><a href="">Armani</a></li>
                                        <li><a href="">Prada</a></li>
                                        <li><a href="">Dolce and Gabbana</a></li>
                                        <li><a href="">Chanel</a></li>
                                        <li><a href="">Gucci</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#womens">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Womens
                                    </a>
                                </h4>
                            </div>
                            <div id="womens" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="">Fendi</a></li>
                                        <li><a href="">Guess</a></li>
                                        <li><a href="">Valentino</a></li>
                                        <li><a href="">Dior</a></li>
                                        <li><a href="">Versace</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Kids</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Fashion</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Households</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Interiors</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Clothing</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Bags</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Shoes</a></h4>
                            </div>
                        </div>
                    </div><!--/category-products-->

                    <div class="brands_products"><!--brands_products-->
                        <h2>Brands</h2>
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href=""> <span class="pull-right">(50)</span>Acne</a></li>
                                <li><a href=""> <span class="pull-right">(56)</span>Grüne Erde</a></li>
                                <li><a href=""> <span class="pull-right">(27)</span>Albiro</a></li>
                                <li><a href=""> <span class="pull-right">(32)</span>Ronhill</a></li>
                                <li><a href=""> <span class="pull-right">(5)</span>Oddmolly</a></li>
                                <li><a href=""> <span class="pull-right">(9)</span>Boudestijn</a></li>
                                <li><a href=""> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
                            </ul>
                        </div>
                    </div><!--/brands_products-->

                    <div class="price-range"><!--price-range-->
                        <h2>Price Range</h2>
                        <div class="well">
                             <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                             <b>$ 0</b> <b class="pull-right">$ 600</b>
                        </div>
                    </div><!--/price-range-->

                    <div class="shipping text-center"><!--shipping-->
                        <img src="images/home/shipping.jpg" alt="" />
                    </div><!--/shipping-->
                </div>
            </div>
            <div class="col-sm-9">
                <div class="blog-post-area">
                    <h2 class="title text-center">Latest From our Blog</h2>

                    <div class="single-blog-post" data-id = "{{ $dataBlog->id_blog }}">
                        <input class = "hidden login" type="text" value="{{ Auth::check() }}">

                        <h3>{{ $dataBlog->title_blog }}</h3>
                        <div class="post-meta">
                            <ul>
                                <li><i class="fa fa-user"></i> {{ $dataBlog->author_blog }}</li>
                                <li><i class="fa fa-clock-o"></i> {{ $dataBlog->created_at->format('H.i') }}</li>
                                <li><i class="fa fa-calendar"></i> {{ $dataBlog->created_at->format('m.d.,Y') }}</li>
                            </ul>
                            <span>
                                <div class="rate">
                                    <div class="vote">

                                            <div class="star_1 ratings_stars"><input value="1" type="hidden"></div>
                                            <div class="star_2 ratings_stars"><input value="2" type="hidden"></div>
                                            <div class="star_3 ratings_stars"><input value="3" type="hidden"></div>
                                            <div class="star_4 ratings_stars"><input value="4" type="hidden"></div>
                                            <div class="star_5 ratings_stars"><input value="5" type="hidden"></div>
                                            <span class="rate-np">{{ $rate }}</span>
                                            <input type="text" class = "hidden rating" value="{{ $rate }}">

                                    </div>
                                </div>
                            </span>
                        </div>
                        <a href="">
                            <img src="{{ asset("upload/blog/image/")}}/{{ $dataBlog->img_blog }}" alt="">
                        </a>
                        <p>{!! $dataBlog->content_blog !!}</p>

                        <div class="pager-area">
                            <ul class="pager pull-right">
                                {{-- @if(session()->has('previous')) --}}
                                @if(isset($previous))
                                    <li><a href="{{ $previous }}">Pre</a></li>

                                @endif
                                @if(isset($next))
                                <li><a href="{{ $next }}">Next</a></li>
                            @endif
                            </ul>
                        </div>

                    </div>
                </div><!--/blog-post-area-->

                <div class="rating-area">
                    <ul class="ratings">
                        <li class="rate-this">Rate this item:</li>
                        <li>
                            <i class="fa fa-star color"></i>
                            <i class="fa fa-star color"></i>
                            <i class="fa fa-star color"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                        </li>
                        <li class="color">(6 votes)</li>
                    </ul
                    <ul class="tag">
                        <li>TAG:</li>
                        <li><a class="color" href="">Pink <span>/</span></a></li>
                        <li><a class="color" href="">T-Shirt <span>/</span></a></li>
                        <li><a class="color" href="">Girls</a></li>
                    </ul>
                </div><!--/rating-area-->

                <div class="socials-share">
                    <a href=""><img src="images/blog/socials.png" alt=""></a>
                </div><!--/socials-share-->

                <div class="media commnets">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="{{ asset('frontend/images/blog/man-one.jpg') }}" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading">Annie Davis</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
                        <div class="blog-socials">
                            <ul>
                                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                                <li><a href=""><i class="fa fa-dribbble"></i></a></li>
                                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                            </ul>
                            <a class="btn btn-primary" href="">Other Posts</a>
                        </div>
                    </div>
                </div>
                <!--Comments-->
                {{-- @include('frontend.blog.comment') --}}
                <!--/Response-area-->

                    @include('frontend.blog.comment',['comments' => $comments])


                <div class="replay-box">
                    <div class="row">

                            <h2>Leave a replay</h2>


                        <div class="col-sm-12">
                            <div class="text-area">
                                <form action="" method="post">
                                    <div class="blank-arrow">
                                        <label>Your Name</label>
                                    </div>
                                    <span>*</span>
                                    <textarea name="message" rows="11" class = 'message'></textarea>
                                    <a class="btn btn-primary post_comment">post comment</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div><!--/Repaly Box-->
            </div>
        </div>
    </div>
</section>
<script>


    $(document).ready(function () {
        //vote
        $rating = $('.rating').val();
        var isLogin = $('.login').val();
        // console.log($rating);
        function removeRate(){
            $('.ratings_stars').each(function(){
                if(this.hasClass('.ratings_hover')){
                    $(this).removeClass('ratings_hover');
                }

            })

        }
        //<!--reply-->



        //<!--post_comment-->
        $('.post_comment').click(function(){
            var content = $('.message').val();
            // console.log(content);
            var _commentUrl = "{{ url("blog-single-comment",$dataBlog->id_blog)}}";
            // console.log({{ $dataBlog->id_blog }});
            if(isLogin)
            {
                $.ajax({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : 'POST',
                    url: _commentUrl,
                    data : {

                        'content' : content,
                    },
                    success:function(data){
                        console.log(data);
                        // $('')
                        // console.log(data);
                        $('.response-area').html(data);

                    }

                })
            }else{
                Swal.fire({
                icon: 'error',
                title: 'Sorry...',
                text: 'Ban chua dang nhap',
                footer: '<a href="{{ url('member/login') }}">Toi trang dang nhap de tiep tuc danh gia?</a>'
                })
            }



        })
        //<!--rate-->
        $('.ratings_stars').hover(

            function () {
                // removeRate();
                $(this).prevAll().andSelf().addClass('ratings_hover');

                // $(this).nextAll().removeClass('ratings_vote');
            },
            function () {
                $(this).prevAll().andSelf().removeClass('ratings_hover');
                // set_votes($(this).parent());
                // function rate();
                rate();
            }
        );
        function rate(){
            $('.ratings_stars').each(function(){
                var x = $(this).find('input').val();
                // console.log(x);
                if($rating == x){
                    $(this).prevAll().andSelf().addClass('ratings_over');
                }

            })

        };





        $('.ratings_stars').click(function () {
            // removerate();
            var Values = $(this).find("input").val();
            $(".rate-np").text(Values);
            var idBlog = $('.single-blog-post').data('id');

            console.log(idBlog);

            if(isLogin){
                console.log('haideptrai');
                if ($(this).hasClass('ratings_over')) {
                    $('.ratings_stars').removeClass('ratings_over');
                    $(this).prevAll().andSelf().addClass('ratings_over');
                } else {
                    $(this).prevAll().andSelf().addClass('ratings_over');
                }
                $.ajax({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : 'post',
                    url:"{{ url('blog-single-rate') }}",
                    data : {
                        'rate' : Values,
                        'id_blog' : idBlog,
                    },
                    success:function(data){
                        console.log(data);
                        // $('')
                        $('.response-area').html(data);

                    }

                })

            } else{
                Swal.fire({
                icon: 'error',
                title: 'Sorry...',
                text: 'Ban chua dang nhap',
                footer: '<a href="{{ url('member/login') }}">Toi trang dang nhap de tiep tuc danh gia?</a>'
                })
            }

        });


        rate();

    });
$(document).on('click', '.reply', function(){
    var isLogin = $('.login').val();
        // console.log('asd');
    var id = $(this).data('id');
    var form_reply = '.form-reply-' +id;
    $('.formReply').slideUp();
    $(form_reply).slideDown();


})
$(document).on('click', '.reply-comment', function(){
    var id = $(this).data('id');
    var isLogin = $('.login').val();
                var _replyUrl = "{{ url("blog-single-comment",$dataBlog->id_blog)}}";

                var reply_content_id = '.reply-comm-' + id;
                // console.log(reply_content_id);
                var reply_content = $(reply_content_id).val();

                if(isLogin)
                {
                    $.ajax({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : 'POST',
                        url: _replyUrl,
                        data : {
                            'reply_id' : id,
                            'content' : reply_content,
                        },
                        success:function(data){
                            console.log(data);
                            // $('')
                            // console.log(data);
                            $('.response-area').html(data);

                        }

                    })
                    }else{
                    Swal.fire({
                    icon: 'error',
                    title: 'Sorry...',
                    text: 'Ban chua dang nhap',
                    footer: '<a href="{{ url('member/login') }}">Toi trang dang nhap de tiep tuc danh gia?</a>'
                    })
                }
            })
</script>
@endsection
