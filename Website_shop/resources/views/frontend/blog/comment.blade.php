<div class="response-area">
    <h2>{{ $comments->count() }} RESPONSES</h2>
    @foreach ($comments as $comm)
        <ul class="media-list">

            <li class="media">



                <a class="pull-left" href="#">
                    <img class="media-object" src="{{ asset('images/blog/man-two.jpg') }}" alt="">
                </a>
                <div class="media-body">
                    <ul class="sinlge-post-meta">
                        <li><i class="fa fa-user"></i>{{ $comm->user->name }}</li>
                        <li><i class="fa fa-clock-o"></i> {{ $comm->created_at->format('H : m') }}</li>
                        <li><i class="fa fa-calendar"></i> {{ $comm->created_at->format('m d , Y') }} </li>
                    </ul>

                    <p>{{ $comm->content }}</p>
                    <a class="btn btn-primary reply" data-id="{{ $comm->id }}"><i class="fa fa-reply"></i>Replay</a>
                </div>
                <form action="" style="display:none" method="post"
                    class="formReply form-reply-{{ $comm->id }}" data-reply-id="{{ $comm->id }}">



                    {{-- <input name = "user_reply_id" value="{{ Auth::user()->id }}" class = "hidden"> --}}

                    <span>Tra loi binh luan</span>
                    <textarea name="message" rows="3" class='reply-comm-{{ $comm->id }}'></textarea>
                    <a class="btn btn-primary reply-comment" data-id="{{ $comm->id }}"><i
                            class="fa fa-reply"></i>Replay</a>
                </form>
            </li>




            @foreach ($comm->reply as $reply)
                <li class="media second-media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="images/blog/man-three.jpg" alt="">
                    </a>
                    <div class="media-body">
                        <ul class="sinlge-post-meta">
                            <li><i class="fa fa-user"></i>{{ $reply->user->name }}</li>
                            <li><i class="fa fa-clock-o"></i> {{ $reply->created_at->format('H : m') }} </li>
                            <li><i class="fa fa-calendar"></i> {{ $reply->created_at->format('m d , Y') }}</li>
                        </ul>
                        <p>{{ $reply->content }}</p>

                    </div>
                </li>
            @endforeach


        </ul>
    @endforeach
</div>
