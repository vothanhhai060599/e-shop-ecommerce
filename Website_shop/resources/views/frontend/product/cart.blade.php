@extends('frontend.layout')
@section('content')
    <section id="cart_items">
        <div class="container">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <table class="table table-condensed">
                    <thead>
                        <tr class="cart_menu">
                            <td class="image">Item</td>
                            <td class="description"></td>
                            <td class="price">Price</td>
                            <td class="quantity">Quantity</td>
                            <td class="total">Total</td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody id ="cart">
                        {{-- @php
                            $carts = session()->get('carts');
                        @endphp --}}

                        @if ($carts=session()->get('carts'))
                            @php
                                $thanh_tien = 0;
                            @endphp
                        @foreach ($carts as $cart)
                            @php

                                // echo $cart['cart_qty'];
                                $total = (int)$cart['cart_price'] * (int)$cart['cart_qty'];
                                // echo $total . ";";
                                $thanh_tien += (int)$total;
                                // echo $thanh_tien . ";";
                            @endphp
                         <tr>

                            <td class="cart_product">
                                <a href=""><img src="{{ $cart['cart_image'] }}" style="height: 50px; width:50px;"
                                        alt=""></a>
                            </td>
                            <td class="cart_description">
                                <h4><a href="">{{ $cart['cart_name'] }}</a></h4>
                                <p>Web ID: {{ $cart['cart_id'] }}</p>
                            </td>

                            <td class="cart_price">
                                <p>${{ $cart['cart_price'] }}</p>
                            </td>
                            <td class="cart_quantity">
                                <div class="cart_quantity_button">
                                    <form method="POST">
                                        @csrf

                                        <a class="cart_quantity_up" data-id="{{  $cart['cart_id'] }}"> + </a>
                                        <input class="cart_quantity_input qty_{{  $cart['cart_id'] }}" type="text"
                                            name="quantity" value="{{  $cart['cart_qty'] }}" autocomplete="off"
                                            size="2">
                                        <a class="cart_quantity_down" data-id="{{  $cart['cart_id'] }}"> - </a>
                                    </form>
                                </div>
                            </td>

                            <td class="cart_total">
                                <p class="cart_total_price total_{{  $cart['cart_id'] }}">${{ $total }}</p>
                            </td>
                            <td class="cart_delete">
                                <a class="cart_quantity_delete delete_{{  $cart['cart_id'] }}"
                                    data-id="{{  $cart['cart_id'] }}"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>

                        @endforeach
                        @endif



                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>What would you like to do next?</h3>
                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your
                    delivery cost.</p>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="chose_area">
                        <ul class="user_option">
                            <li>
                                <input type="checkbox">
                                <label>Use Coupon Code</label>
                            </li>
                            <li>
                                <input type="checkbox">
                                <label>Use Gift Voucher</label>
                            </li>
                            <li>
                                <input type="checkbox">
                                <label>Estimate Shipping & Taxes</label>
                            </li>
                        </ul>
                        <ul class="user_info">
                            <li class="single_field">
                                <label>Country:</label>
                                <select>
                                    <option>United States</option>
                                    <option>Bangladesh</option>
                                    <option>UK</option>
                                    <option>India</option>
                                    <option>Pakistan</option>
                                    <option>Ucrane</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>

                            </li>
                            <li class="single_field">
                                <label>Region / State:</label>
                                <select>
                                    <option>Select</option>
                                    <option>Dhaka</option>
                                    <option>London</option>
                                    <option>Dillih</option>
                                    <option>Lahore</option>
                                    <option>Alaska</option>
                                    <option>Canada</option>
                                    <option>Dubai</option>
                                </select>

                            </li>
                            <li class="single_field zip-field">
                                <label>Zip Code:</label>
                                <input type="text">
                            </li>
                        </ul>
                        <a class="btn btn-default update" href="">Get Quotes</a>
                        <a class="btn btn-default check_out" href="">Continue</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="total_area">
                        <ul>
                            <li>Cart Sub Total <span>$59</span></li>
                            <li>Eco Tax <span>$2</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            @if (session()->get('carts'))
                                <li>Total <span class="thanh_tien">${{ $thanh_tien }}</span></li>
                            @else
                                <li>Total <span class="thanh_tien">$0</span></li>
                            @endif

                        </ul>
                        <a class="btn btn-default update" href="">Update</a>
                        <a class="btn btn-default check_out" href="">Check Out</a>



                        @if (Auth::check())
                            <a class="btn btn-default order" type="button" href="{{ route('order') }}">Order</a>
                        @else
                            <a class="btn btn-default  per-order" data-target="#login" data-toggle="modal"
                                href="">Order</a>
                            <div id="login" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                    <div class="modal-content">
                                        <div class="modal-body">
                                            <button data-dismiss="modal" class="close">&times;</button>
                                            <div class="container-form" id="container">
                                                <div class="form-container sign-up-container">
                                                    <form action="" id = "signup">
                                                        @csrf
                                                        <h1>Create Account</h1>
                                                        <div class="social-container">
                                                            <a href="#" class="social"><i
                                                                    class="fab fa-facebook-f"></i></a>
                                                            <a href="#" class="social"><i
                                                                    class="fab fa-google-plus-g"></i></a>
                                                            <a href="#" class="social"><i
                                                                    class="fab fa-linkedin-in"></i></a>
                                                        </div>
                                                        <span>or use your email for registration</span>
                                                        <input type="text" placeholder="Name" name = "name" class = "name"/>
                                                        <input type="email" placeholder="Email" name = "email"  class = "email"/>
                                                        <input type="password" placeholder="Password" name = "password"  class = "password"/>
                                                        <button type = "button" class = "signup">Sign Up</button>
                                                        <p class = "noti-signup"></p>
                                                        {{-- <a class="btn btn-default signup">Check Out</a> --}}
                                                        {{-- <a class = "signup">Sign Up</a> --}}
                                                    </form>
                                                    <script>
                                                    $(function(){
                                                        // $("#signup").submit(function(e){
                                                        //     e.preventDefault();
                                                        //     // console.log("asd");
                                                        //     let formData = new FormData(this);
                                                        //     var name = formData.getAll('name','email','password');
                                                        //     // var email = formData.getAll('email');
                                                        //     // var password = formData.getAll('password');
                                                        //     // formData = formData.getAll();
                                                        //     console.log(name);
                                                        //     $.ajax({
                                                        //         headers: {
                                                        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                        //         },
                                                        //         type : "POST",
                                                        //         url : "{{ url('register-order-cart') }}",
                                                        //         data: {
                                                        //            'data' : formData,
                                                        //         },
                                                        //         success: function(data) {
                                                        //             // console.log(data);
                                                        //             // $(".asd").html(data);
                                                        //         }
                                                        //     })




                                                        // })
                                                        $(".signup").click(function(){
                                                            var name = $(".name").val();
                                                            // console.log(name);
                                                            var email = $(".email").val();
                                                            var password = $(".password").val();
                                                            $.ajax({
                                                                    headers: {
                                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                                    },
                                                                    type: "POST",
                                                                    url: "{{ url('register-order-cart') }}",
                                                                    data: {
                                                                        'name' : name,
                                                                        'email' : email,
                                                                        'password' : password,
                                                                    },
                                                                    success: function(data) {
                                                                        // console.log(data['success']);
                                                                        // $(".asd").html(data);
                                                                        $(".noti-signup").text(data['success']);
                                                                    }
                                                            })
                                                        })
                                                    })

                                                    </script>
                                                </div>
                                                <div class="form-container sign-in-container">
                                                    <form action="{{ route('login.order.cart') }}" id = "signin" method = "POST">
                                                        @csrf
                                                        <h1>Sign in</h1>
                                                        <div class="social-container">
                                                            <a href="#" class="social"><i
                                                                    class="fab fa-facebook-f"></i></a>
                                                            <a href="#" class="social"><i
                                                                    class="fab fa-google-plus-g"></i></a>
                                                            <a href="#" class="social"><i
                                                                    class="fab fa-linkedin-in"></i></a>
                                                        </div>
                                                        <span>or use your account</span>
                                                        <input type="email" placeholder="Email" name = "email"/>
                                                        <input type="password" placeholder="Password" name = "password"/>
                                                        <a>Forgot your password?</a>
                                                        <button>Sign In</button>
                                                    </form>
                                                </div>
                                                <div class="overlay-container">
                                                    <div class="overlay">
                                                        <div class="overlay-panel overlay-left">
                                                            <h1>Welcome Back!</h1>
                                                            <p>To keep connected with us please login with your personal
                                                                info</p>
                                                            <button class="ghost" id="signIn">Sign In</button>
                                                        </div>
                                                        <div class="overlay-panel overlay-right">
                                                            <h1>Hello, Friend!</h1>
                                                            <p>Enter your personal details and start journey with us</p>
                                                            <button class="ghost" id="signUp">Sign Up</button>
                                                            {{-- @if () --}}
                                                            {{-- @if(session('success'))


                                                             @endif --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        <input class="hidden login" type="text" value="{{ Auth::check() }}">

                    </div>
                </div>
                @if (session('error.order'))
                    <script>
                        $(function() {
                            aler('asd');

                        })
                    </script>

                @endif
            </div>
        </div>
    </section>
    <!--/#do_action-->
    <script>
        $(function() {
            $(".cart_quantity_up").click(function() {
                // console.log('sad');
                var id = $(this).data('id');
                var qty = $(".qty_" + id).val();
                var price = parseInt($(this).closest("tr").find(".cart_price").find("p").text().replace("$",
                    ""));
                // alert(price);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ url('qty-up-ajax') }}",
                    data: {
                        'cart_id': id,
                        'cart_qty': qty,

                    },
                    success: function(data) {
                        // console.log(data);
                        var total = 0;
                        var thanh_tien = 0;
                        $.each(data['data'], function(key, val) {
                            $(".qty_" + val['cart_id']).val(val['cart_qty']);
                            total = parseInt(val['cart_price']) * val['cart_qty'];
                            $(".total_" + val['cart_id']).text("$" + total);
                            thanh_tien += total;
                        })

                        // console.log(total);
                        $(".thanh_tien").text("$" + thanh_tien);

                    }
                })

            })
            $(".cart_quantity_down").click(function() {
                var id = $(this).data('id');
                var qty = $(".qty_" + id).val();
                var price = parseInt($(this).closest("tr").find(".cart_price").find("p").text().replace("$",
                    ""));
                // alert(price);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ url('qty-down-ajax') }}",
                    data: {
                        'cart_id': id,
                        'cart_qty': qty,

                    },
                    success: function(data) {
                        console.log(data);
                        var total = 0;
                        var thanh_tien = 0;
                        $.each(data['data'], function(key, val) {
                            $(".qty_" + val['cart_id']).val(val['cart_qty']);
                            total = parseInt(val['cart_price']) * val['cart_qty'];
                            $(".total_" + val['cart_id']).text("$" + total);
                            thanh_tien += total;
                        })
                        // var thanh_tien += total;
                        $(".thanh_tien").text("$" + thanh_tien);
                    }
                })
            })
            // $(".cart_quantity_delete").click(function() {

            //     var id = $(this).data('id');
            //     console.log(id);
            //     $.ajax({
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         },
            //         type: "POST",
            //         url: "{{ url('delete-cart-ajax') }}",
            //         data: {
            //             'cart_id': id,
            //         },
            //         success: function(data) {
            //             // console.log(data);
            //             $("#cart").html(data);
            //         }
            //     })

            // })





        })
        $(document).on('click', '.cart_quantity_delete', function(){
            var id = $(this).data('id');
                console.log(id);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type: "POST",
                    url: "{{ url('delete-cart-ajax') }}",
                    data: {
                        'cart_id': id,
                    },
                    success: function(data) {
                        // console.log(data);
                        $("#cart").html(data);
                    }
                })
        })
    </script>
@endsection
