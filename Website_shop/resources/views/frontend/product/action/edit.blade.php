@extends('frontend.layout')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Account</h2>
                        <div class="panel-group category-products" id="accordian">
                            <!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            Account
                                        </a>
                                    </h4>
                                </div>
                                <div id="sportswear" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Nike </a></li>
                                            <li><a href="">Under Armour </a></li>
                                            <li><a href="">Adidas </a></li>
                                            <li><a href="">Puma</a></li>
                                            <li><a href="">ASICS </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a href="{{ route('product') }}">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            My product
                                        </a>
                                    </h4>
                                </div>
                                <div id="mens" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Fendi</a></li>
                                            <li><a href="">Guess</a></li>
                                            <li><a href="">Valentino</a></li>
                                            <li><a href="">Dior</a></li>
                                            <li><a href="">Versace</a></li>
                                            <li><a href="">Armani</a></li>
                                            <li><a href="">Prada</a></li>
                                            <li><a href="">Dolce and Gabbana</a></li>
                                            <li><a href="">Chanel</a></li>
                                            <li><a href="">Gucci</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-7">
                    <p style="font-size:20px; ">Update Product</p>
                    <form action="{{ route('edit.product',[$product->id]) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">

                            <input type="text" class="form-control" name="name" value="{{ $product->name }}"
                                placeholder="Name">
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">

                            <input type="text" class="form-control" name="price" value="{{ $product->price }}"
                                placeholder="Price">
                            @error('price')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">

                            <select class="form-control form-control-line" name="category">
                                @foreach ($categories as $cate)
                                    <option value="{{ $cate->id }}"
                                        <?= $product->category_id == $cate->id ? 'selected' : '' ?>>{{ $cate->category }}
                                    </option>
                                @endforeach
                            </select>
                            @error('category')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">

                            <select class="form-control form-control-line" name="brand">
                                @foreach ($brands as $brand)
                                    {{-- <option value=""></option> --}}
                                    <option value="{{ $brand->id }}"
                                        <?= $product->brand_id == $brand->id ? 'selected' : '' ?>>{{ $brand->brand }}
                                    </option>
                                @endforeach
                            </select>
                            @error('brand')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">

                            <select class="form-control form-control-line " id="status" name="status">
                                @if ($product->status == 0)
                                    <option value="sale">sale</option>
                                    <option>new</option>
                                @else
                                    <option>new</option>
                                    <option value="sale">sale</option>
                                @endif


                                {{-- <option >

                                </option> --}}

                            </select>
                            @error('status')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group" id="sale">

                            <input type="number" name="sale" value="{{ $product->sale }}" placeholder="0">%


                        </div>



                        <div class="form-group">

                            <input id="files" type="file" class="form-control" name="filename[]" multiple="multiple"
                                accept="image/*" multiple>

                            <div id="preview">
                                <div class = "row" style="
                                margin-top: 2%;">
                                @foreach ($getArrImage as $img)
                                    <div class="col-lg-3">
                                       <ul>
                                            <li>

                                                <img src="{{ asset('upload/product/' . $img) }}" alt="">
                                            </li>
                                            <li style="margin-top:10%; margin-left: 75%">
                                                <input type="checkbox" name="check_image[]"  value ="{{ $img }}" style="transform: scale(2);">
                                            </li>

                                       </ul>
                                    </div>
                                @endforeach
                                </div>
                            </div>

                        </div>
                        <div class="form-group">

                            <input type="text" class="form-control" name="company" value="{{ $product->company }}"
                                placeholder="Company profile">
                        </div>
                        <div class="form-group">

                            <textarea name="detail" id="" cols="30" rows="10"  placeholder="Detail">{{ $product->detail }}</textarea>
                            @error('detail')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-default">Create product</button>

                        </div>

                    </form>
                </div>

            </div>
    </section>
    <script>
        $(function() {
            $('#sale').hide();
            $('#status').change(function() {
                if ($('#status').val() == 'sale') {
                    $('#sale').show();
                } else {
                    $('#sale').hide();
                }
            });


        })
    </script>
@endsection
