@extends('frontend.layout')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Account</h2>
                        <div class="panel-group category-products" id="accordian">
                            <!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            Account
                                        </a>
                                    </h4>
                                </div>
                                <div id="sportswear" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Nike </a></li>
                                            <li><a href="">Under Armour </a></li>
                                            <li><a href="">Adidas </a></li>
                                            <li><a href="">Puma</a></li>
                                            <li><a href="">ASICS </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a  href="{{ route('product') }}">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            My product
                                        </a>
                                    </h4>
                                </div>
                                <div id="mens" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Fendi</a></li>
                                            <li><a href="">Guess</a></li>
                                            <li><a href="">Valentino</a></li>
                                            <li><a href="">Dior</a></li>
                                            <li><a href="">Versace</a></li>
                                            <li><a href="">Armani</a></li>
                                            <li><a href="">Prada</a></li>
                                            <li><a href="">Dolce and Gabbana</a></li>
                                            <li><a href="">Chanel</a></li>
                                            <li><a href="">Gucci</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-7">
                    <p style="font-size:20px; ">Create Product</p>
                    <form action="{{ route('add.product') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">

                            <input type="text" class="form-control" name="name" value="" placeholder="Name">
                            @error('name')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">

                            <input type="text" class="form-control" name="price" value="" placeholder="Price">
                            @error('price')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">

                            <select class="form-control form-control-line" name = "category" >
                                @foreach ($categories as $cate )


                                <option value = "{{$cate->id}}"


                                    >{{ $cate->category }}</option>

                                @endforeach
                            </select>
                            @error('category')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">

                            <select class="form-control form-control-line" name = "brand" >
                                @foreach ($brands as $brand )

                                {{-- <option value=""></option> --}}
                                <option value = "{{$brand->id}}"


                                    >{{ $brand->brand }}</option>

                                @endforeach
                            </select>
                            @error('brand')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">

                            <select class="form-control form-control-line " id="status" name = "status" >
                                <option>new</option>
                                <option value="sale">sale</option>
                            </select>
                            @error('status')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group" id ="sale">

                            <input type="number" name="sale" value="" placeholder="0">%
                        </div>
                        <div class="form-group">

                            <input type="text" class="form-control" name="company" value="" placeholder="Company profile">
                        </div>


                        <div class="form-group">
                            {{-- <img id="avatar-preview" src="" />
                            <img id="avatar-preview" src="" />
                            <input id="file-input" type="file" class="form-control" name="filename[]" multiple ="multiple"> --}}
                            <input id="files" type="file" class="form-control"  name="filename[]" multiple ="multiple" accept="image/*" multiple>
                            <div id="preview">

                            </div>
                            {{-- @error('image')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror --}}


                        </div>
                        <div class="form-group">

                            <textarea name="detail" id="" cols="30" rows="10" placeholder="Detail"></textarea>
                            @error('detail')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-default">Create product</button>

                        </div>
                    </form>
                </div>

            </div>
    </section>
    <script>
        $(function(){
            $('#sale').hide();
            $('#status').change(function(){
                if($('#status').val() == 'sale'){
                    $('#sale').show();
                }else{
                    $('#sale').hide();
                }
            })
        })
    </script>
@endsection
