<div class="table-responsive cart_info">
    <table class="table table-condensed">
        <thead>
            <tr class="cart_menu">
                <td class="image">Item</td>
                <td class="description"></td>
                <td class="price">Price</td>
                <td class="quantity">Quantity</td>
                <td class="total">Total</td>
                <td></td>
            </tr>
        </thead>
        <tbody id ="cart">
            @php
                $thanh_tien = 0;
            @endphp
            @foreach ($carts as $cart)
                @php

                    $total = (int)$cart['cart_price'] * (int)$cart['cart_qty'];
                    // echo $total;
                    $thanh_tien += (int)$total;
                @endphp
                 <tr>
                    <td class="cart_product">
                        <a href=""><img src="{{ $cart['cart_image'] }}" style="height: 50px; width:50px;"
                                alt=""></a>
                    </td>
                    <td class="cart_description">
                        <h4><a href="">{{ $cart['cart_name'] }}</a></h4>
                        <p>Web ID: {{ $cart['cart_id'] }}</p>
                    </td>

                    <td class="cart_price">
                        <p>${{ $cart['cart_price'] }}</p>
                    </td>
                    <td class="cart_quantity">
                        <div class="cart_quantity_button">
                            <form method="POST">
                                @csrf

                                <a class="cart_quantity_up" data-id="{{  $cart['cart_id'] }}"> + </a>
                                <input class="cart_quantity_input qty_{{  $cart['cart_id'] }}" type="text"
                                    name="quantity" value="{{  $cart['cart_qty'] }}" autocomplete="off"
                                    size="2">
                                <a class="cart_quantity_down" data-id="{{  $cart['cart_id'] }}"> - </a>
                            </form>
                        </div>
                    </td>

                    <td class="cart_total">
                        <p class="cart_total_price total_{{  $cart['cart_id'] }}">$<?= $total ?></p>
                    </td>
                    <td class="cart_delete">
                        <a class="cart_quantity_delete delete_{{  $cart['cart_id'] }}"
                            data-id="{{  $cart['cart_id'] }}"><i class="fa fa-times"></i></a>
                    </td>
                </tr>
            @endforeach


        </tbody>
    </table>
    <span class="thanh_tien">Thanh tien : ${{ $thanh_tien }} </span>
</div>
