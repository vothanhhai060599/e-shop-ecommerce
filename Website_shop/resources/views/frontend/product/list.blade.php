@extends('frontend.layout')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Account</h2>
                        <div class="panel-group category-products" id="accordian">
                            <!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            Account
                                        </a>
                                    </h4>
                                </div>
                                <div id="sportswear" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Nike </a></li>
                                            <li><a href="">Under Armour </a></li>
                                            <li><a href="">Adidas </a></li>
                                            <li><a href="">Puma</a></li>
                                            <li><a href="">ASICS </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="{{ route('product') }}">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            My product
                                        </a>
                                    </h4>
                                </div>
                                <div id="mens" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Fendi</a></li>
                                            <li><a href="">Guess</a></li>
                                            <li><a href="">Valentino</a></li>
                                            <li><a href="">Dior</a></li>
                                            <li><a href="">Versace</a></li>
                                            <li><a href="">Armani</a></li>
                                            <li><a href="">Prada</a></li>
                                            <li><a href="">Dolce and Gabbana</a></li>
                                            <li><a href="">Chanel</a></li>
                                            <li><a href="">Gucci</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
                {{-- <div class="col-sm-9">
                    <table class="table">
                        <thead>
                          <tr>
                            <th scope="col">id</th>
                            <th scope="col">Name</th>
                            <th scope="col">Image</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                          </tr>
                        </thead>
                        <tbody>


                        </tbody>
                      </table>
                </div> --}}
                <div class="col-sm-9">

                    <table class="table table-striped table-class " id= "table-id">




                        <thead style="background: #FE980F;">
                            <tr style="color: #fff">
                                <th scope="col" class="pr-5">id</th>
                                <th scope="col" class="pr-5">Name</th>
                                <th scope="col" class="pr-5">Price</th>
                                <th scope="col" class="pr-5">Image</th>
                                <th scope="col" class="pr-5">Detail</th>
                                <th scope="col" >Action</th>
                              </tr>
                        </thead>

                        <tbody>
                            @foreach ($products as $product )
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->price }}$</td>
                                <td><img src="{{ asset('upload/product/')}}.{{ $product->image }}" alt=""></td>
                                <td>{{ $product->detail }}</td>
                                <td>
                                <a href="{{ route('show.edit.product',[$product->id]) }}"><i class="far fa-edit " style="margin-right: 20px;"></i></a>

                                <a href=""><i class="far fa-trash-alt"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>

                    </table>
                </div>
                <div class ="row">
                    <div class ="col-sm-11"></div>
                    <div class="col-sm-1">

                            <form method="post" enctype="multipart/form-data">

                                <button type="submit" class="btn btn-default" style = "background: #FE980F;" name = "add"><a href ="{{  route('show.add.product')  }}">add new</a></button>

                            </form>

                    </div>
                </div>

            </div>
    </section>
@endsection
