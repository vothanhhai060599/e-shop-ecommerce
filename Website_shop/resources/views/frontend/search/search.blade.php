@extends('frontend.layout')
@section('content')
<section id="advertisement">
    <div class="container">
        <img src="images/shop/advertisement.jpg" alt="" />
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Category</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Sportswear
                                    </a>
                                </h4>
                            </div>
                            <div id="sportswear" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="">Nike </a></li>
                                        <li><a href="">Under Armour </a></li>
                                        <li><a href="">Adidas </a></li>
                                        <li><a href="">Puma</a></li>
                                        <li><a href="">ASICS </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#mens">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Mens
                                    </a>
                                </h4>
                            </div>
                            <div id="mens" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="">Fendi</a></li>
                                        <li><a href="">Guess</a></li>
                                        <li><a href="">Valentino</a></li>
                                        <li><a href="">Dior</a></li>
                                        <li><a href="">Versace</a></li>
                                        <li><a href="">Armani</a></li>
                                        <li><a href="">Prada</a></li>
                                        <li><a href="">Dolce and Gabbana</a></li>
                                        <li><a href="">Chanel</a></li>
                                        <li><a href="">Gucci</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordian" href="#womens">
                                        <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                        Womens
                                    </a>
                                </h4>
                            </div>
                            <div id="womens" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul>
                                        <li><a href="">Fendi</a></li>
                                        <li><a href="">Guess</a></li>
                                        <li><a href="">Valentino</a></li>
                                        <li><a href="">Dior</a></li>
                                        <li><a href="">Versace</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Kids</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Fashion</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Households</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Interiors</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Clothing</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Bags</a></h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><a href="#">Shoes</a></h4>
                            </div>
                        </div>
                    </div><!--/category-productsr-->

                    <div class="brands_products"><!--brands_products-->
                        <h2>Brands</h2>
                        <div class="brands-name">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href=""> <span class="pull-right">(50)</span>Acne</a></li>
                                <li><a href=""> <span class="pull-right">(56)</span>Grüne Erde</a></li>
                                <li><a href=""> <span class="pull-right">(27)</span>Albiro</a></li>
                                <li><a href=""> <span class="pull-right">(32)</span>Ronhill</a></li>
                                <li><a href=""> <span class="pull-right">(5)</span>Oddmolly</a></li>
                                <li><a href=""> <span class="pull-right">(9)</span>Boudestijn</a></li>
                                <li><a href=""> <span class="pull-right">(4)</span>Rösch creative culture</a></li>
                            </ul>
                        </div>
                    </div><!--/brands_products-->

                    <div class="price-range"><!--price-range-->
                        <h2>Price Range</h2>
                        <div class="well">
                             <input type="text" class="span2" value="" data-slider-min="0" data-slider-max="600" data-slider-step="5" data-slider-value="[250,450]" id="sl2" ><br />
                             <b>$ 0</b> <b class="pull-right">$ 600</b>
                        </div>

                    </div>
                    <script>
                        // var min = 0;
                        // $(function(){
                        //     $(".slider-selection").click(function(){
                        //         var price_range = $(".tooltip-inner").text();
                        //     console.log(price_range);
                        //     })
                        // })
                        function priceRange(){
                            var price_range = $(".tooltip-inner").text();
                            console.log(price_range);
                            var range = price_range.split(":");
                            console.log(range[0]);
                            var min = range[0];
                            var max = range[1];
                            console.log(min);
                            $.ajax({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type: "POST",
                                url: "{{ url('price-range-ajax') }}",
                                data: {
                                    'min' : min,
                                    'max' : max
                                },
                                success: function(data) {
                                    // console.log(data);
                                    $(".features_items").html(data);
                                    // $("#cart").html(data);
                                }
                            })
                        }


                    </script>
                    <!--/price-range-->
                    {{-- <script>
                        $(function(){
                            $(".span2").click(function(){
                                // var slider_min = $(this).data('slider-min');
                                var slider_value = $(this).data('slider-value');
                                $.ajax({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                    },
                                    type: "POST",
                                    url: "{{ url('price-range-ajax') }}",
                                    data: {
                                        'value' : slide_value
                                    },
                                    success: function(data) {
                                        // console.log(data);
                                        // $(".features_items").html(data);
                                        // $("#cart").html(data);
                                    }
                                })
                                // console.log(slider_value);
                            })
                        })
                    </script> --}}
                    <div class="shipping text-center"><!--shipping-->
                        <img src="images/home/shipping.jpg" alt="" />
                    </div><!--/shipping-->

                </div>
            </div>

            <div class="col-sm-9 padding-right">

                <div class="features_items"><!--features_items-->
                    <h2 class="title text-center">Features Items</h2>

                    <div style="margin-right: -9%;">
                        <input type="text" name = "name" class = "name-product" style="width: 18%;padding: 7px;" placeholder="name">

                        <select name="" id="" style="width: 18%" class = "price-product">
                            <option value="0">Choose price</option>
                            <option value="1">1  -  100</option>
                            <option value="2">100  -  500</option>
                            <option value="3">500  -  1000</option>
                            <option value="4">  >  1000</option>
                        </select>
                        <select name="" id="" style="width: 18%" class = "category-product">
                            <option value="">Category</option>
                            @foreach ($categories as $cate)


                            <option value="{{ $cate->id }}">
                                {{ $cate->category }}
                            </option>
                            @endforeach
                        </select>
                        <select name="" id="" style="width: 18%" class = "brand-product">
                            <option value="">Brand</option>
                            @foreach ($brands as $brand)


                            <option value="{{ $brand->id }}">
                                {{ $brand->brand }}
                            </option>
                            @endforeach
                        </select>
                        <select name="" id="" style="width: 18%" class = "status-product">
                            <option value="">Status</option>
                            <option value="0">sale</option>
                            <option value="1">new</option>
                        </select>

                    </div>
                    <div style="margin-top: 1%; margin-bottom: 1%;">
                        <a  class = "btn btn-default search">Search</a>
                    </div>




                    {{-- <ul class="pagination">
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">&raquo;</a></li>
                    </ul> --}}
                </div><!--features_items-->
            </div>
        </div>
    </div>
</section>
<script>

    $(document).on('click', '.search', function(){
        var name = $(".name-product").val();
            var price = $(".price-product :selected").val();
            var category = $(".category-product :selected").val();
            var brand = $(".brand-product :selected").val();
            var status = $(".status-product :selected").val();
            // console.log(status);
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url: "{{ url('search-advanced-ajax') }}",
                data: {
                    'name' : name,
                    'price' : price,
                    'category' : category,
                    'brand' : brand,
                    'status' : status,
                },
                success: function(data) {
                    console.log(data);
                    // if(data['message']){

                    // }
                    $(".features_items").html(data);
                    // $("#cart").html(data);
                }
            })
    })
    // $(document).on('click', '.slider-selection', function(){
    //     var price_range = $(".tooltip-inner").text();
    //     console.log(price_range);

    // })


</script>
@endsection
