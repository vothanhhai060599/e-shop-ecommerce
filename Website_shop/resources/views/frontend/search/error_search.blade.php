


<h2 class="title text-center">Features Items</h2>

<div style="margin-right: -9%;">
    <input type="text" name = "name" class = "name-product" style="width: 18%;padding: 7px;" placeholder="name">

    <select name="" id="" style="width: 18%" class = "price-product">
        <option value="0">Choose price</option>
        <option value="1">1  -  100</option>
        <option value="2">100  -  500</option>
        <option value="3">500  -  1000</option>
        <option value="4">  >  1000</option>
    </select>
    <select name="" id="" style="width: 18%" class = "category-product">
        <option value="">Category</option>
        @foreach ($categories as $cate)


        <option value="{{ $cate->id }}">
            {{ $cate->category }}
        </option>
        @endforeach
    </select>
    <select name="" id="" style="width: 18%" class = "brand-product">
        <option value="">Brand</option>
        @foreach ($brands as $brand)


        <option value="{{ $brand->id }}">
            {{ $brand->brand }}
        </option>
        @endforeach
    </select>
    <select name="" id="" style="width: 18%" class = "status-product">
        <option value="">Status</option>
        <option value="1">new</option>

        <option value="0">sale</option>


    </select>

</div>
<div style="margin-top: 1%; margin-bottom: 1%;">
    <a  class = "btn btn-default search">Search</a>
</div>

<div class="col-sm-12">
    @if (session('thongbao'))
        <div style="margin-top: 5%;">
            <img style="height: 30%; width: 30%;display: block; margin-left: auto; margin-right: auto;" src="{{ asset('frontend/images/404/404.png') }}" alt="">
            <p style="text-align: center;">Không tìm thấy kết quả nào</p>
            <p style="text-align: center;">Hãy thử sử dụng các từ khóa chung chung hơn</p>
        </div>
    @endif
</div>
