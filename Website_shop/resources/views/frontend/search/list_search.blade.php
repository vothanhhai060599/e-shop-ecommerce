
<h2 class="title text-center">Features Items</h2>

<div style="margin-right: -9%;">
    <input type="text" name = "name" class = "name-product" style="width: 18%;padding: 7px;" placeholder="name">

    <select name="" id="" style="width: 18%" class = "price-product">
        <option value="0">Choose price</option>
        <option value="1">1  -  100</option>
        <option value="2">100  -  500</option>
        <option value="3">500  -  1000</option>
        <option value="4">  >  1000</option>
    </select>
    <select name="" id="" style="width: 18%" class = "category-product">
        <option value="">Category</option>
        @foreach ($categories as $cate)


        <option value="{{ $cate->id }}">
            {{ $cate->category }}
        </option>
        @endforeach
    </select>
    <select name="" id="" style="width: 18%" class = "brand-product">
        <option value="">Brand</option>
        @foreach ($brands as $brand)


        <option value="{{ $brand->id }}">
            {{ $brand->brand }}
        </option>
        @endforeach
    </select>
    <select name="" id="" style="width: 18%" class = "status-product">
        <option value="">Status</option>
        <option value="1">new</option>

        <option value="0">sale</option>


    </select>

</div>
<div style="margin-top: 1%; margin-bottom: 1%;">
    <a  class = "btn btn-default search">Search</a>
</div>
@foreach ($search as $product)


<div class="col-sm-4">
    <div class="product-image-wrapper">
        <div class="single-products">
            <div class="productinfo text-center">
                {{-- @php
                    $images = json_decode($product->images);
                    foreach($images as $key=>$img){
                        print_r ($img);
                    }
                @endphp --}}
                <img src="{{ $product->image }}" alt="" />
                <h2>${{ $product->price }}</h2>
                <p>{{ $product->name }}</p>
                <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
            </div>
            <div class="product-overlay">
                <div class="overlay-content">
                    <h2>${{ $product->price }}</h2>
                    <p>{{ $product->name }}</p>
                    <a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                </div>
            </div>
        </div>
        <div class="choose">
            <ul class="nav nav-pills nav-justified">
                <li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                <li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
            </ul>
        </div>
    </div>
</div>
@endforeach
