@extends('frontend.layout')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <div class="left-sidebar">
                        <h2>Account</h2>
                        <div class="panel-group category-products" id="accordian">
                            <!--category-productsr-->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordian" href="#sportswear">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            Account
                                        </a>
                                    </h4>
                                </div>
                                <div id="sportswear" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Nike </a></li>
                                            <li><a href="">Under Armour </a></li>
                                            <li><a href="">Adidas </a></li>
                                            <li><a href="">Puma</a></li>
                                            <li><a href="">ASICS </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a  href="{{ route('product') }}">
                                            <span class="badge pull-right"><i class="fa fa-plus"></i></span>
                                            My product
                                        </a>
                                    </h4>
                                </div>
                                <div id="mens" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul>
                                            <li><a href="">Fendi</a></li>
                                            <li><a href="">Guess</a></li>
                                            <li><a href="">Valentino</a></li>
                                            <li><a href="">Dior</a></li>
                                            <li><a href="">Versace</a></li>
                                            <li><a href="">Armani</a></li>
                                            <li><a href="">Prada</a></li>
                                            <li><a href="">Dolce and Gabbana</a></li>
                                            <li><a href="">Chanel</a></li>
                                            <li><a href="">Gucci</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>





                        </div>
                    </div>

                </div>
                <div class="col-sm-2"></div>
                <div class="col-sm-7">
                    <p style="font-size:20px; ">User Update</p>
                    <form action="{{ route ('update.account') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">User name</label>
                            <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" class="form-control" name="email" value="{{ $user->email }}">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label for="">Phone</label>
                            <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                        </div>
                        <div class="form-group">
                            <label for="">Address</label>
                            <input type="text" class="form-control" name="address" value="{{ $user->address }}">
                        </div>
                        <div class="form-group">
                            <label for="">Country</label>
                            <select class="form-control form-control-line" name = "id_country" >
                                @foreach ($country as $value )


                                <option value = "{{$value->id_country}}"
                                    <?=
                                       Auth::user()->id_country == $value->id_country ? "selected" : "";

                                    ?>

                                    >{{$value->name_country}}</option>

                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Avatar</label>
                            <img id="avatar-preview" src="" />
                            <input id="file-input" type="file" class="form-control" name="avatar"
                                value="{{ $user->avatar }}">



                        </div>

                        <div class="form-group">

                            <button type="submit" class="btn btn-default">Create product</button>

                        </div>
                    </form>
                </div>

            </div>
    </section>
@endsection
