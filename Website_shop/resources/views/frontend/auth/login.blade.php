@extends('frontend.layout')
@section('content')
<section id="form"><!--form-->
    <div class="container">
        <div class="row">
            <div class="col-sm-5 col-sm-offset-1">
                <div class="login-form"><!--login form-->
                    <h2>Login to your account</h2>
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                            {{session('success')}}
                        </div>

                    @endif
                    @if(session('errors'))
                        <div class="alert alert-danger alert-dismissible">
                            <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Dang nhap that bai</h4>
                            {{session('success')}}
                        </div>

                    @endif
                    @if($errors->any())
                        <h4>{{$errors->first('message')}}</h4>
                    @endif
                    <form action="{{ url('login') }}" method = "post">
                        @csrf
                        <input type="email" placeholder="email" name = "email"/>
                        @error('email')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input type="password" placeholder="password" name = "password" />
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <span>
                            <input type="checkbox" class="checkbox" name = "remember_me">
                            Keep me signed in

                        </span>

                        <button type="submit" class="btn btn-default">Login</button>
                        <a href="">Quen mat khau?</a>
                        <a href="{{ url('register') }}">Register</a>

                    </form>
                </div><!--/login form-->
            </div>

        </div>
    </div>
</section><!--/form-->
@endsection
