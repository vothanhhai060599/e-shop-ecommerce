@extends('frontend.layout')
@section('content')
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    {{-- @if(Auth::check()) --}}
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card">
                <div class="card-body">

                </div>
                <div>
                    <hr> </div>
                <div class="card-body">

                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success alert-dismissible">
                            <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                            {{session('success')}}
                        </div>

                        @endif
                    <form class="form-horizontal form-material" action = "{{url('register')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label class="col-md-12">Full Name</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="" name = "name" class="form-control form-control-line" >
                                @error('name')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>


                        </div>
                        <div class="form-group">
                            <label for="example-email" class="col-md-12">Email</label>
                            <div class="col-md-12">
                                <input   type="email" placeholder="" name = "email" class="form-control form-control-line" name="example-email" id="example-email" >
                                @error('email')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Password</label>
                            <div class="col-md-12">
                                <input type="password" name = "password" class="form-control form-control-line" >
                                @error('password')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Phone</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="" name = "phone" class="form-control form-control-line" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Address</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="" name = "address" class="form-control form-control-line" >
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <input class = "hidden" readonly placeholder="" name = "level" class="form-control form-control-line" value ="0" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Avatar</label>
                            <div class="col-md-12">
                                <img id="avatar-preview" src="" />
                                <input id="file-input" type="file" class="form-control" name="avatar">
                                @error('admin_avatar')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>

                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success">Create</button>
                            </div>
                        </div>


                        {{-- <script>

                            Toastify({
                                text: "I'm a toast",
                                duration: 3000,
                                destination: 'https://github.com/apvarun/toastify-js',
                                newWindow: true,
                                close: true,
                                gravity: "top", // `top` or `bottom`
                                positionLeft: true, // `true` or `false`
                                backgroundColor: "linear-gradient(to right, #00b09b, #96c93d)"
                            }).showToast();

                        </script> --}}
                        {{-- @endif
                        @if(session('alert'))

                        @endif
                        @if($errors->any())
                        <h4>{{$errors->first()}}</h4>
                        @endif --}}
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    {{-- @endif --}}
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
@endsection
