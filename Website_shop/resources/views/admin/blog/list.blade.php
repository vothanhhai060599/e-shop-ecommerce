@extends('admin.layout')
@section('content_main')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h1 class="page-title">Blog</h1>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        
        

	    <div class="container">
            @if(session('add'))
            <div class="alert alert-success alert-dismissible">
                <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('add')}}
            </div>
            
            @endif
            @if(session('update'))
            <div class="alert alert-success alert-dismissible">
                <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('update')}}
            </div>
            
            @endif
            @if(session('delete'))
            <div class="alert alert-success alert-dismissible">
                <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                {{session('delete')}}
            </div>
            
            @endif
        <h5>choose to watch</h5>
            <div class="form-group"> 	
                <select class  ="form-control" name="state" id="maxRows">
                    <option value="5000">Show ALL Rows</option>
                    <option value="1">1</option>
                    <option value="3">3</option>
                    <option value="5">5</option>
                    <option value="7">7</option>
                    <option value="9">9</option>
                    
                </select>
                    
            </div>

        <table class="table table-striped table-class " id= "table-id">
         
           
              
            
            <tr>
              <th scope="col" class="pr-5">#</th>
              <th scope="col" class="pr-5">Title</th>
              <th scope="col" class="pr-5">Author</th>
              <th scope="col" class="pr-5">Image</th>
              <th scope="col" class="pr-5">Description</th>
              <th scope="col" >Action</th>
            </tr>
            @foreach ($blog as $value )
            <tr>
              <td>{{$value->id_blog}}</td>
              <td>{{$value->title_blog}}</td>
              <td>{{$value->author_blog}}</td>
              <td>{{$value->img_blog}}</td>
              <td>{{$value->content_blog}}</td>
              <td>
                <a href="{{asset("admin/blog/update-blog")}}/{{$value->id_blog}}"><i class="mdi mdi-24px mdi-pencil mr-5"></i></a>
                <a href="{{asset("admin/blog/delete-blog")}}/{{$value->id_blog}}"><i class="mdi mdi-24px mdi-delete"></i></a>  
              </td>
            </tr>
            @endforeach
        </table>

        <!--Start Pagination -->
        <div class='pagination-container' >
          <nav>
            <ul class="pagination">
              
              <li data-page="prev" >
                       <span>Previous<span class="sr-only">(current)</span></span>
                      </li>
             <!--	Here the JS Function Will Add the Rows -->
          <li data-page="next" id="prev">
                         <span>Next <span class="sr-only">(current)</span></span>
                      </li>
            </ul>
          </nav>
        </div>
        <div>
          <a class="btn btn-info btn-rounded btn-sm" href = "{{url("/admin/blog/add-blog")}}">Create blog<i
            class="fas fa-plus-square ml-1"></i></a>
        </div>

      </div>
       <!--End of Container -->
       @if(session('alert'))
        <script>

            Toastify({

            text: "them thanh cong",

            duration: 3000,
            position: "center"
            }).showToast();

        </script>
        @endif
        @if(session('alertDelete'))
       
        <script>
            alert('xoa thanh cong');
            Toastify({

            text: "them thanh cong",

            duration: 3000,
            position: "center"
            }).showToast();

        </script>
        @endif



<!--  Developed By Yasser Mas -->

           
          

          
           
         
          
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection