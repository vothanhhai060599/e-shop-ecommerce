@extends('admin.layout')
@section('content_main')
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Update Blog</h4>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex align-items-center justify-content-end">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="#">Home</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page">Update Blog</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <!-- Row -->
        @if(Auth::check())
        <div class="row">
            <!-- Column -->
            
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-body">
                        @foreach ($blog as $value )
                        <form class="form-horizontal form-material" action = "{{url('admin/blog/update-blog')}}/{{$value->id_blog}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Title<span style = "color:red;">(*)</span></label> 
                                <div class="col-md-12">
                                    <input type="text" placeholder="" name = "title_blog" class="form-control form-control-line" value = "{{$value->title_blog}}">
                                    @error('title_blog')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                               
                                
                            </div>
                            <div class="form-group">
                                <label for="example-email" class="col-md-12">Author</label>
                                <div class="col-md-12">
                                    <input type="text" placeholder="" name = "author_blog" class="form-control form-control-line" name="example-email" id="example-email" value ="{{$value->author_blog}}">
                                    @error('author_blog')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                  
                            </div>
                            <div class="form-group">
                                <label class="col-md-12">Image</label>
                                <div class="col-md-12">
                                    <img id="avatar-preview" src="{{asset('upload/blog/image')}}/{{$value->img_blog}}" />
                                    <input id="file-input" type="file" class="form-control" name="img_blog">
                                    @error('img_blog')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                
                            </div> 
                            <div class="form-group">
                                <label class="col-md-12">Content</label>
                                <div class="col-md-12">
                                    <textarea name="content_blog" id="demo" cols="30" rows="10" >{{$value->content_blog}}</textarea>
                                </div>
                                @error('content_blog')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
 
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button class="btn btn-success">Update Blog</button>
                                </div>
                            </div>
                            
                            @if(session('alert'))
                            <script>

                                Toastify({

                                text: "Update thanh cong",

                                duration: 3000,
                                position: "center"
                                }).showToast();

                            </script>
                            @endif
                            
                        </form>
                        @endforeach
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        @endif
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- footer -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
    <!-- End footer -->
    <!-- ============================================================== -->
</div>
@endsection