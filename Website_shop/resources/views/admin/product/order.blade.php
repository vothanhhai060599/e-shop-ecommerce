@extends('admin.layout')
@section('content_main')
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h1 class="page-title">Country</h1>
                </div>
                <div class="col-7 align-self-center">
                    <div class="d-flex align-items-center justify-content-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Country</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            {{-- <div class="row">

            <div class="col-12">
                <div class="card">

                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Country</th>
                                    <th scope="col">Description</th>
                                    <th scope="col"colspan = "2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>
                                        <a class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons"></i></a>
                                        <a><button class="btn btn-primary">Delete</button></a>
                                    </td>
                                    <td>
                                        <a><button class="btn btn-primary">Edit</button></a>
                                    </td>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div> --}}


            <div class="container">
                @if (session('add'))
                    <div class="alert alert-success alert-dismissible">
                        <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                        {{ session('add') }}
                    </div>
                @endif
                @if (session('update'))
                    <div class="alert alert-success alert-dismissible">
                        <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                        {{ session('update') }}
                    </div>
                @endif
                @if (session('delete'))
                    <div class="alert alert-success alert-dismissible">
                        <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                        {{ session('delete') }}
                    </div>
                @endif
                <h5>choose to watch</h5>
                <div class="form-group">
                    <select class="form-control" name="state" id="maxRows">
                        <option value="5000">Show ALL Rows</option>
                        <option value="1">1</option>
                        <option value="3">3</option>
                        <option value="5">5</option>
                        <option value="7">7</option>
                        <option value="9">9</option>

                    </select>

                </div>

                <table class="table table-striped table-class " id="table-id">




                    <tr>
                        <th scope="col" class="pr-5">#</th>
                        <th scope="col" class="pr-5">Country</th>
                        <th scope="col" class="pr-5">Description</th>
                        <th scope="col">Action</th>
                    </tr>
                    @foreach ($country as $value)
                        <tr>
                            <td>{{ $value->id_country }}</td>
                            <td>{{ $value->name_country }}</td>
                            <td></td>
                            <td>
                                <a href="{{ url('admin/country/update-country') }}/{{ $value->id_country }}"><i
                                        class="mdi mdi-24px mdi-account-edit mr-5"></i></a>
                                <a href="{{ url('admin/country/delete-country') }}/{{ $value->id_country }}"><i
                                        class="mdi mdi-24px mdi-delete"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </table>

                <!--Start Pagination -->
                <div class='pagination-container'>
                    <nav>
                        <ul class="pagination">

                            <li data-page="prev">
                                <span>Previous<span class="sr-only">(current)</span></span>
                            </li>
                            <!--	Here the JS Function Will Add the Rows -->
                            <li data-page="next" id="prev">
                                <span>Next <span class="sr-only">(current)</span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div>
                    <a class="btn btn-info btn-rounded btn-sm" href="{{ url('/admin/country/add-country') }}">Add country<i
                            class="fas fa-plus-square ml-1"></i></a>
                </div>

            </div>
            <!--End of Container -->
            @if (session('alert'))
                <script>
                    Toastify({

                        text: "them thanh cong",

                        duration: 3000,
                        position: "center"
                    }).showToast();
                </script>
            @endif
            @if (session('alertDelete'))
                <script>
                    alert('xoa thanh cong');
                    Toastify({

                        text: "them thanh cong",

                        duration: 3000,
                        position: "center"
                    }).showToast();
                </script>
            @endif



            <!--  Developed By Yasser Mas -->








            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->
            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>
@endsection
