@extends('admin.layout')
@section('content_main')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-5 align-self-center">
                            <h1 >Brand</h1>
                        </div>
                        <div class="col-7 align-self-center">
                            <div class="d-flex align-items-center justify-content-end">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item">
                                            <a href="#">Home</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Brand</li>
                                        <li class="breadcrumb-item active" aria-current="page">add_Brand</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">

                    <div class="card" style="margin-top:80px; ">
                        <div class="card-body">
                            <form class="form-horizontal form-material" action = "{{ route('add.brand') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-md-12">Name Brand</label>
                                    <div class="col-md-12">
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control form-control-line" name ="brand">
                                        </div>
                                        {{-- @error('name_Brand')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror --}}
                                    </div>


                                </div>


                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Add Brand</button>
                                    </div>
                                </div>



                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
@endsection
