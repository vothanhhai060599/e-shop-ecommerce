@extends('admin.layout')
@section('content_main')
    <div class="page-wrapper">
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h1 class="page-title">Category</h1>
                </div>
                <div class="col-7 align-self-center">
                    <div class="d-flex align-items-center justify-content-end">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="#">Home</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Category</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="container">
                {{-- @if (session('add'))
                <div class="alert alert-success alert-dismissible">
                    <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                    {{ session('add') }}
                </div>
            @endif
            @if (session('update'))
                <div class="alert alert-success alert-dismissible">
                    <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                    {{ session('update') }}
                </div>
            @endif
            @if (session('delete'))
                <div class="alert alert-success alert-dismissible">
                    <button type=" button" class="close" data-dismiss="alert"aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-check"></i> Thông báo!</h4>
                    {{ session('delete') }}
                </div>
            @endif --}}
                <h5>choose to watch</h5>
                <div class="form-group">
                    <select class="form-control" name="state" id="maxRows">
                        <option value="5000">Show ALL Rows</option>
                        <option value="1">1</option>
                        <option value="3">3</option>
                        <option value="5">5</option>
                        <option value="7">7</option>
                        <option value="9">9</option>

                    </select>

                </div>

                <table class="table table-striped table-class " id="table-id">




                    <tr>
                        <th scope="col" class="pr-5">#</th>
                        <th scope="col" class="pr-5">Category</th>
                        <th scope="col" class="pr-5">Description</th>
                        <th scope="col">Action</th>
                    </tr>
                    @foreach ($categories as $cate)
                        <tr>
                            <td>{{ $cate->id }}</td>
                            <td>{{ $cate->category }}</td>
                            <td></td>
                            <td>
                                <a href=""><i class="mdi mdi-24px mdi-account-edit mr-5"></i></a>
                                <a href=""><i class="mdi mdi-24px mdi-delete"></i></a>
                            </td>
                        </tr>
                    @endforeach
                </table>

                <!--Start Pagination -->
                <div class='pagination-container'>
                    <nav>
                        <ul class="pagination">

                            <li data-page="prev">
                                <span>Previous<span class="sr-only">(current)</span></span>
                            </li>
                            <!--	Here the JS Function Will Add the Rows -->
                            <li data-page="next" id="prev">
                                <span>Next <span class="sr-only">(current)</span></span>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div>
                    <a class="btn btn-info btn-rounded btn-sm" href="{{ route('show.add.category') }}">Add Category<i
                            class="fas fa-plus-square ml-1"></i></a>
                </div>

            </div>
            <!--End of Container -->
            {{-- @if (session('alert'))
            <script>
                Toastify({

                    text: "them thanh cong",

                    duration: 3000,
                    position: "center"
                }).showToast();
            </script>
        @endif
        @if (session('alertDelete'))
            <script>
                alert('xoa thanh cong');
                Toastify({

                    text: "them thanh cong",

                    duration: 3000,
                    position: "center"
                }).showToast();
            </script>
        @endif --}}













        </div>

    </div>
@endsection
