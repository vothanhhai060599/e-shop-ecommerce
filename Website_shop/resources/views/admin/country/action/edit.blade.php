@extends('admin.layout')
@section('content_main')
    <div class="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="page-breadcrumb">
                    <div class="row">
                        <div class="col-5 align-self-center">
                            <h1 >Country</h1>
                        </div>
                        <div class="col-7 align-self-center">
                            <div class="d-flex align-items-center justify-content-end">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item">
                                            <a href="#">Home</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Country</li>
                                        <li class="breadcrumb-item active" aria-current="page">add_country</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="">
                    
                    <div class="card" style="margin-top:80px; ">
                        <div class="card-body">
                            @foreach ($country as $value)
                            <form class="form-horizontal form-material" action = "{{url('admin/country/update-country')}}/{{$value->id_country}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group">
                                    <label class="col-md-12">Name Country</label> 
                                    <div class="col-md-12">
                                        <div class="col-sm-12">
                                             
                                                
                                            
                                            <input name = "name_country" class="form-control form-control-line" value = "{{$value->name_country}}" > 
                                            
                                        </div>
                                        @error('name_country')
                                            <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                   
                                    
                                </div>
                                
                               
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Add country</button>
                                    </div>
                                </div>
                                
                                
                                
                            </form>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        
       
    </div>
@endsection