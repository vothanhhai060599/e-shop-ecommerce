<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;
    protected $table = 'comments';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id','user_id','content_comment','blog_id','reply_id',
    ];

    public $timestamps = true;
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function blog(){
        return $this->belongsTo(Blog::class,'blog_id');
    }
    public function reply(){
        return $this->hasMany(Comment::class,'reply_id');
    }
}
