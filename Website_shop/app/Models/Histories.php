<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Histories extends Model
{
    use HasFactory;
    protected $table = 'histories';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'name', 'price', 'id_user', 'phone', 'email'
    ];
    public $timestamps = true;

    public function user(){
        return $this->belongsTo(User::class);
    }
}
