<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rate extends Model
{
    use HasFactory;
    protected $table = 'rate';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id','id_blog', 'rating',
    ];

    public $timestamps = true;
}
