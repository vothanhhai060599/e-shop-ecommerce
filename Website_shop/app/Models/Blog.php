<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $table = 'blog';
    protected $primaryKey = 'id_blog';
    protected $fillable = [
        'id_blog','title_blog', 'author_blog', 'img_blog', 'content_blog'
    ];

    public $timestamps = true;
    // protected $dateFormat = 'U';
    protected $dates = ['date_begin', 'date_end'];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comment(){
        return $this->hasMany(Comment::class)->whereNull('reply_id');
    }
    

}
