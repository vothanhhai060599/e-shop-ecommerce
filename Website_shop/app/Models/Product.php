<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'name', 'price', 'brand_id', 'category_id', 'status', 'sale', 'company', 'images', 'detail'
    ];
    public $timestamps = true;

    public function category(){
        return $this->hasOne(Category::class,'category_id');
    }
    public function brand(){
        return $this->hasOne(Category::class,'brand_id');
    }
    //mutator - lay 1 image
    public function getImageAttribute(){
        $fileImage = Arr::first(json_decode($this->images));
        return asset('upload/product/' . $fileImage);

    }
    public function getImgAttribute(){
        $fileImage = Arr::first(json_decode($this->images));
        return $fileImage;
    }
    public function scopeName($q, $search)
    {
        return $q->where('name', 'like', "%$search->name%");
    }
    public function scopeStatus($q, $search)
    {
        return $q->where('status', $search->status);
    }
    public function scopeCategories($q, $search)
    {
        return $q->where('category_id', $search->category);
    }
    public function scopeBrands($q, $search)
    {
        return $q->where('brand_id', $search->brand);
    }
    public function scopePrice($q, $search)
    {

    }
    //mutator get images in products
    // public function getImagesAttribute(){
    //     $fileImages =  json_decode($this->images);
    //     return asset('upload/product/' . $fileImages);

    // }


}
