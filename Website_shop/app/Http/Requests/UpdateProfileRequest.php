<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif|max:1024'
        ];
    }

    public function messages()
    {
        return [
            'required' => ':attribute khong duoc de trong',
            'image' => ':attribute phai co dinh dang jpeg, png, jpg va gif',
            'max' => ':attribute phai co do lon khong qua 1mb'
        ];
    }
    
    public function attributes()
    {
        return [
            
        ];
    }
}
