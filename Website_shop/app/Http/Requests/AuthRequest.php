<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */


    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|max:11',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif|max:1024'
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute khong duoc bo trong',
            'max' => ':attribute khong duoc qua 11 ky tu',
            'image' => ':attribute phai thuoc dinh dang anh',
        ];

    }
}
