<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required",
            'price' => 'required|integer',
            'category' => 'required',
            'brand' => 'required',
            'image'=> 'image|mimes:jpeg,png,jpg,gif|max:1024'
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute khong duoc de trong',
            'integer' => ':attribute phai la so',
            'image' => ':attribute phai co dinh dang jpeg, png, jpg va gif',
            'max' => ':attribute phai co do lon khong qua 1mb'
        ];
    }
}
