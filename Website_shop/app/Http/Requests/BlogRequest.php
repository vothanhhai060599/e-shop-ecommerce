<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_blog' => 'required',
            'author_blog' => 'string',
            'img_blog' => 'image|mimes:jpeg,jpg,png,gif|max:1024',
            'content_blog' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'required' => ':attribute khong duoc de trong',
            'string' => ':attribute phai nhap kieu chu',
            'image' => ':attribute phai nhap dinh dang kieu anh',
            'max' => ':attribute khong duoc qua 1mb',
        ];
    }

    public function attributes()
    {
        return [
            'title_blog' => 'Tieu de',
            'author_blog' => 'Ten tac gia',
            'img_blog' => 'Anh',
            'content_blog' => 'Noi dung',
        ];
    }
}
