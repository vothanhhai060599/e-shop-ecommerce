<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    //view country
    public function index()
    {
        $country = Country::all();
        // dd($country);
        return view('admin.country.list',compact('country'));
    }
    //request country
    public function countryRequest($check)
    {
        $this->validate($check,
            [
                'name_country' => 'required|string',
            ],
            [
                'required' => ':attribute khong duoc de trong',
                'string' => ':attribute nhap kieu chu'
            ],
            [
                'name_country' => 'Country'
            ]
        );
    }
    //start add country
    public function showAddCountry()
    {
        
        return view('admin.country.action.add');
    }
    
    public function addCountry(Request $request)
    {
        $this->countryRequest($request);
        $country = new country();
        $country->name_country = $request->name_country;
        $country->save();
        
        return redirect('admin/country')->with('add','them thanh cong');
    }
    //end add country

    //start update country
    public function showUpdateCountry($id_country)
    {
        $country = Country::where('id_country',$id_country)->get();
        
        // dd($country);
        return view('admin.country.action.edit', compact('country'));
    }
    public function updateCountry(Request $request)
    {
        $this->countryRequest($request);
        $name_country = $request->name_country;
        Country::where('id_country',$request->id_country)
        ->update(['name_country' => $name_country]);
        return redirect('admin/country')->with('update', 'update thanh cong');
    }
    public function deleteCountry(Request $request){
        Country::where('id_country',$request->id_country)->delete();
        return redirect('admin/country')->with('delete','Xoa thanh cong');
    }

    // public function showCountry()
    // {
    //     $country = DB::table('country')->get();
    //     return view('admin.profile.list', compact('country'));
    // }
    //end update country
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
