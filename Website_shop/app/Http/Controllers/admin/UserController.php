<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Country;
use App\Models\Comment;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\UpdateProfileRequest;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showProfile(Request $request)
    {
        $userId = Auth::id();
        $country = Country::all();
        // $countryId = DB::table('country')->where('name_country',$request->id_country)->select('id_country')->get();
        // DB::table('users')->where('id',$userId)->update(['id_country',$countryId]);
        return view('admin.profile.list', compact('country'));
    }
    public function updateProfile(UpdateProfileRequest $request)
    {
        $userId = Auth::id();

        $user = User::findOrFail($userId);

        $data = $request->all();

        // dd($data);
        $image = $request->avatar;
        // dd($image);

        if(!empty($image))
        {
            $file = $image->getClientOriginalName();
            $data['avatar'] = $file;
            $image->move('upload/user/avatar', $file);
        }

        if($data['password'])
        {

            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }


        if($user->update($data))
        {
            // session()->flash('success', 'Todo created successfully.');
            return redirect()->back()->with('success',__('update profile sucess'));
        }

        // return view('admin.dashboard.menu.profile');
    }
    // public function comment()
    // {
    //     return $this->hasMany(Comment::class);
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
