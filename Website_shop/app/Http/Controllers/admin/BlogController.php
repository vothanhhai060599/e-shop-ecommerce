<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showBlog()
    {
        $blog = Blog::all(); 
        
        return view('admin.blog.list', compact('blog'));
    }
    public function showAddBlog()
    {
        
        return view('admin.blog.action.add');
    }
    public function addBlog(BlogRequest $request)
    {
        $data = $request->all();
        $image = $request->img_blog;
        $blog = new blog();
        $blog->title_blog = $data['title_blog'];
        if(!empty($image)){
            $file = $image->getClientOriginalName();
            $blog->img_blog = $file;
            $image->move('upload/blog/image', $file);
        }
        $blog->author_blog = $data['author_blog'];
        
        $blog->content_blog = $data['content_blog'];
        $blog->save();
        return redirect('admin/blog')->with('add', 'them thanh cong');
    }
    public function showUpdateBlog(Request $request)
    {
        $blog = Blog::where('id_blog',$request->id_blog)->get();
        // dd($blog);
        return view('admin.blog.action.edit', compact('blog'));
    }
    public function updateBlog(BlogRequest $request)
    {
        // dd($request->id_blog);
        $data = $request->all();
        $blogId = $request->id_blog;
        $blog = Blog::find($blogId);
        $img_blog = $request->img_blog;
        if(!empty($img_blog))
        {
            $file = $img_blog->getClientOriginalName();
            $data['img_blog'] = $file;
            $img_blog->move('upload/blog/image', $file);
        }else{
            $data['img_blog'] = $blog->img_blog;
        }
         
        $blog->title_blog = $data['title_blog'];
        $blog->author_blog = $data['author_blog'];
        $blog->img_blog = $data['img_blog'];
        $blog->content_blog = $data['content_blog'];
        $blog->save();

        return redirect('admin/blog')->with('update','Update thanh cong');
     
    }
    public function deleteBlog(Request $request)
    {
        $blogId = $request->id_blog;
        $blog = Blog::find($blogId);
        $blog->delete();
        return redirect('admin/blog')->with('delete','Update thanh cong');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
