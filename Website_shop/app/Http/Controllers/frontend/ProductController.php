<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use App\Http\Requests\ProductRequest;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // $pathView = 'frontend.product.';
    public function product()
    {
        $products = Product::all();
        // dd($products);
        return view('frontend.product.list', compact('products'));
    }
    public function showAddProduct()
    {
        $categories = Category::all();
        $brands = Brand::all();
        // dd($brands);
        return view('frontend.product.action.add', compact('categories', 'brands'));
    }

    public function image($request)
    {
        if($request->hasfile('filename'))
        {
            foreach($request->file('filename') as $image)
            {

                $name = $image->getClientOriginalName();
                $name_2 = "2".$image->getClientOriginalName();
                $name_3 = "3".$image->getClientOriginalName();

                // $image = $image->move('upload/product/', $name);

                $path = public_path('upload/product/' . $name);
                $path2 = public_path('upload/product/' . $name_2);
                $path3 = public_path('upload/product/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(50, 70)->save($path2);
                Image::make($image->getRealPath())->resize(200, 300)->save($path3);
                $data[] = $name;

            }
        }
        return $data;
    }
    public function addProduct(Request $request)

    {

        // var_dump($this->data);
        $product = new Product();
        // $this->image($request);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        if($request->sale){
            $product->sale = $request->sale;
            $product->status = 0;
        }else{
            $product->sale = 0;
            $product->status = 1;
        }
        $product->company = $request->company;
        $product->detail = $request->detail;

        $product->images=json_encode($this->image($request));
        $product->save();
        return redirect()->route('product');
    }
    public function showEditProduct($id)
    {
        $categories = Category::all();
        $brands = Brand::all();
        $product = Product::where('id', $id)->first();
        $getArrImage = json_decode($product['images'], true);
        // dd($getArrImage);
        return view('frontend.product.action.edit',compact('categories', 'brands', 'product', 'getArrImage'));
    }
    public function editProduct(Request $request)
    {
        $selectedImgs = $request->input('check_image');
        $product = Product::where('id', $request->id)->first();
        $getArrImage = json_decode($product['images'], true);
        $product = Product::find($request->id);
        $product->name = $request->name;
        $product->price = $request->price;
        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        if($request->sale){
            // dd('1');
            $product->sale = $request->sale;
            $product->status = 0;
        }else{
            // dd('2');
            $product->sale = 0;
            $product->status = 1;
        }
        if($request->has('check_image')){
            if($request->has('filename')){
                $check = count($getArrImage) - count($selectedImgs) + count($this->image($request));
                // dd($check);
                if($check > 3){
                    return redirect()->back();
                }else{
                    $arrayImages = array_diff($getArrImage, $selectedImgs);
                    $array = array_merge($arrayImages, $this->image($request));
                    $product->images = json_encode($array);
                    // dd($data);

                }
            }else{

                $arrayImages = array_diff($getArrImage, $selectedImgs);
                // dd(json_encode($arrayImages));
                $product->images = json_encode($arrayImages);
            }
        }else{
            if($request->has('filename')){
                // dd($getArrImage);
                $check = count($getArrImage) + count($this->image($request));
                if($check > 3){
                    return redirect()->back();
                }else{


                    $array = array_merge($getArrImage, $this->image($request));
                    // dd($data);
                    $product->images = json_encode($array);

                }
            }else{

            }
        }
        $product->company = $request->company;
        $product->detail = $request->detail;
        $product->save();
        return redirect()->route('product');

    }

    public function detail($id_product)
    {
        $product = Product::where('id', $id_product)->first();
        // dd($product['brand_id']);
        $brand = Brand::where('id', $product['brand_id'])->first();
        // dd($brand);
        $getArrImage = json_decode($product['images'], true);
        // dd($getArrImage[0]);
        return view('frontend.product.detail', compact('product', 'getArrImage','brand'));
    }
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
