<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Histories;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailOrder;
session_start();


class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function cart(Request $request)
    {
        // session_destroy();
        // session()->forget('cart');

        $data = $request->all();

        $cart = array(
            'cart_id' => $data['cart_id'],
            'cart_name' => $data['cart_name'],
            'cart_image' => $data['cart_image'],
            'cart_price' => $data['cart_price'],
            'cart_qty' => $data['cart_qty'],
        );
        // dd($cart);
        $carts = session()->get('carts');
        // dd($carts);
        if(session()->has('carts'))
        {

            // dd($getSessionCart);
            $is_avaiable = 1;
            foreach($carts as $key => $val){
                // dd($key);

                if($val['cart_id'] == $data['cart_id']){

                    // dd($getSessionCart[$key]['cart_qty']);
                    $carts[$key]['cart_qty'] = (int)$carts[$key]['cart_qty'] + 1;
                    $is_avaiable = 0;
                    session()->put('carts', $carts);

                }
            }
            if($is_avaiable == 1){

                session()->push('carts', $cart);

            }

        }else{

            session()->push('carts', $cart);

        }
        // dd($carts);
        // $view =  view('frontend.product.cart', compact('carts'))->render();
        return response()->json(['success' => 'them thanh cong', 'data' => $carts]);
        // dd($carts);
        // return view('frontend.product.cart', compact('carts'))->render();


    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showCart()
    {
        return view('frontend.product.cart');
    }
    /**
     * Display a listing of the resource.
     * qty up when click up
     * @return \Illuminate\Http\Response
     */
    public function cartQtyUp(Request $request)
    {
        $data = $request->all();
        // // dd($data);
        $carts = session()->get('carts');
        foreach($carts as $key => $val){
            if($val['cart_id'] == $data['cart_id']){
                $carts[$key]['cart_qty'] = (int)$carts[$key]['cart_qty'] + 1;
                session()->put('carts', $carts);
            }
        }
        return response()->json(['data' => $carts]);

    }
    /**
     * Display a listing of the resource.
     * qty down when click down
     * @return \Illuminate\Http\Response
     */
    public function cartQtyDown(Request $request)
    {
        $data = $request->all();
        // // dd($data);
        $carts = session()->get('carts');
        foreach($carts as $key => $val){
            if($val['cart_id'] == $data['cart_id']){
                $carts[$key]['cart_qty'] = (int)$carts[$key]['cart_qty'] - 1;
                session()->put('carts', $carts);
                if($carts[$key]['cart_qty'] == 0){
                    unset($cart[$key]);
                    session()->put('carts', $carts);
                }
            }
        }
        return response()->json(['data' => $carts]);

    }
    /**
     * Display a listing of the resource.
     * delete cart
     * @return \Illuminate\Http\Response
     */
    public function deleteCart(Request $request)
    {
        $id = $request->cart_id;
        // dd($id);

        $carts = session()->get('carts');
        foreach($carts as $key => $val){
            if($val['cart_id'] == $id){
                unset($carts[$key]);

                session()->put('carts', $carts);
            }
        }
        return view('frontend.product.deletecart',compact('carts'));
        // return response()->json()
    }
    /**
     * Display a listing of the resource.
     * Check permission order
     * @return \Illuminate\Http\Response
     */
    public function perOrder()
    {
        if(Auth::check()){
            echo "asd";
        }else{
            // echo "123";
            return redirect()->back()->with('error-order');
        }
    }
    /**
     * Display a listing of the resource.
     * register order cart
     * @return \Illuminate\Http\Response
     */
    public function quickRegister(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->level = 0 ;
        $user->save();
        return response()->json(['success' => 'dang ky thanh cong']);
    }
    /**
     * Display a listing of the resource.
     * login order cart
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];
        if(Auth::attempt($login))
        {

            return redirect()->back();
            // return redirect()->route('home_frontend')->with('success', 'login ok');

        }else{

        }
    }
    /**
     * Display a listing of the resource.
     * order
     * @return \Illuminate\Http\Response
     */
    public function order()
    {
        $carts = session()->get('carts');

        $total = 0 ;
        $amount = 0 ;
        foreach($carts as $val){

            $total = $val['cart_qty'] * (int)$val['cart_price'];
            $amount += $total;
        }
        // dd($amount);
        $user = Auth::user();
        // dd($user);
        $histories = new Histories();
        $histories->name = $user->name;
        $histories->email = $user->email;
        $histories->id_user = $user->id;
        if($user->phone){
            $histories->phone = $user->phone;
        }
        $histories->price = $amount;
        $histories->save();
        $mailable = new MailOrder($carts);
        Mail::to("vothanhhai060599@gmail.com")->send($mailable);
        return redirect()->back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
