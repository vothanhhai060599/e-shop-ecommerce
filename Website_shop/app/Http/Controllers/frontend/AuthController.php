<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\AuthRequest;
use App\Http\Requests\LoginRequest;
use App\Models\User;
use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLogin ()
    {
       return view('frontend.auth.login');
    }
    public function showRegister()
    {
        return view ('frontend.auth.register');
    }
    public function postRegister(AuthRequest $request)
    {
        $data = $request->all();


        $members = new user();
        $members->name = $data['name'];
        $members->email = $data['email'];
        $members->phone = $data['phone'];
        $members->password = bcrypt($data['password']);
        $members->level = 0 ;

        // dd($members);
        $members->save();


        return redirect()->route('login');

    }
    public function checkLogin(LoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];
        $remember = false;
        if($request->remember_me)
        {
            $remember = true;
        }
        if(Auth::attempt($login, $remember))
        {

            return redirect() -> intended(RouteServiceProvider::HOME_CLIENT);
            // return redirect()->route('home_frontend')->with('success', 'login ok');

        }else{
            return redirect()->back()->withErrors('message','Email or password is not correct');
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect()->route('home_frontend');

    }
    public function showAccount()
    {
        $userId = Auth::user()->id;
        // dd($userId);
        $user = User::where('id', $userId)->first();
        $country = Country::all();

        return view('frontend.auth.account',compact('user','country'));
    }
    public function updateAccount(Request $request)
    {
        $userId = Auth::id();
        // dd($userId);
        $user = User::findOrFail($userId);
        // dd($user);

        $data = $request->all();

        // dd($data);
        $image = $request->avatar;
        // dd($image);

        if(!empty($image))
        {
            $file = $image->getClientOriginalName();
            $data['avatar'] = $file;
            $image->move('upload/user/avatar', $file);
        }

        if($data['password'])
        {

            $data['password'] = bcrypt($data['password']);
        }else{
            $data['password'] = $user->password;
        }


        if($user->update($data))
        {
            // session()->flash('success', 'Todo created successfully.');
            return redirect()->back()->with('success',__('update profile sucess'));
        }
    }

    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
