<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Rate;
use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use App\Http\Requests\CommentRequest;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $pathView = 'frontend.blog.';
    public function avgRate()
    {
        $rating = Rate::select('rating')->where('id_blog',$request->id_blog)->get();
        // dd($rating);
        $count = count($rating);
        // echo ($rating);
        $vote = 0;
        echo($count);
        foreach($rating as $key => $value)
        {
            echo ($value);
            $vote += $value;
        }
        echo($vote);
    }
    public function blog()
    {
        $dataBlog = Blog::orderBy('id_blog','DESC')->paginate(3);


        // return redirect()->route('blog');
        return view($this->pathView.'list',compact('dataBlog'));
    }
    public function blogSingle()
    {

        return view($this->pathView.'single');
    }
    public function showBlogSingle($id_blog)
    {
        //comment

        // $userId = 0;
        $comments = Comment::where('blog_id', $id_blog)->where('reply_id', 0)->with('user:id,name')->orderBy('id', 'ASC')->get();
        // $userId = User::where()
        $reply = Comment::where('blog_id',$id_blog)->with('user:id,name','reply.user:id,name')->get();
        $responses = count(Comment::where('blog_id', $id_blog)->get());

        $rating = Rate::where('id_blog',$id_blog)->get();
        $count = count($rating);
        $rate= 0 ;
        if($count > 0)
        {
            foreach($rating as $value){
                $rate = $rate + $value['rating'] ;
            }
            $rate = round($rate / $count) ;
        }
        echo $rate;


        //showBlog
        $dataBlog = Blog::where('id_blog',$id_blog)->first();

        // get previous user id
        $previous = Blog::where('id_blog','<',$id_blog)->max('id_blog');

        // get next user id
        $next = Blog::where('id_blog', '>', $id_blog)->min('id_blog');


        return view($this->pathView.'single',compact('dataBlog','rate','comments','reply','responses'))->with('previous', $previous)->with('next', $next);
    }

    public function rate(Request $request)
    {

        $rating = $request->rate;
        $idBlog = $request->id_blog;
        // echo $rating;
        // echo $idBlog;
        $rate = new rate();
        $rate->rating = $rating;
        $rate->id_blog = $idBlog;
        $rate->save();
    }
    public function showComment()
    {


    }

    public function comment($id_blog , Request $request)
    {

        $userId = Auth::user()->id;
        $comment = new Comment();
        $comment->user_id = $userId;
        $comment->blog_id = $id_blog;
        $comment->content = $request->content;
        if($request->reply_id){
            $comment->reply_id = $request->reply_id;
        }else{
            $comment->reply_id = 0;
        }

        $comment->save();
        $comments = Comment::where('blog_id', $id_blog)->where('reply_id', 0)->with('user:id,name')->orderBy('id', 'ASC')->get();

        // dd($response);
        return view($this->pathView.'comment',compact('comments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
