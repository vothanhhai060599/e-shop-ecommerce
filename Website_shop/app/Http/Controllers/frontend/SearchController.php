<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     * Show view seach advanced
     * @return \Illuminate\Http\Response
     */
    public function searchAdvanced()
    {
        $brands = Brand::all();
        $categories = Category::all();
        $products = Product::all();
        // dd($categories);
        // dd($products);
        return view("frontend.search.search", compact('brands', 'categories', 'products'));
    }
    /**
     * Display a listing of the resource.
     * seach advanced with ajax
     * @return \Illuminate\Http\Response
     */
    public function searchAdvancedAjax(Request $request)
    {
        $brands = Brand::all();
        $categories = Category::all();
        $products = Product::query();
        // dd( Product::all());
        if($request->name){
            $products->where('name', 'like', "%$request->name%");
        }
        if($request->status == 0){
            $products->where('status', 0);

        }else{
            $products->where('status', 1);
        }
        if($request->category){
            $products->where('category_id', $request->category);
        }
        if($request->brand){
            $products->where('brand_id', $request->brand);
        }

        if($request->price == 1){
            $products->whereBetween('price', [1, 100]);
        }
        if($request->price == 2){
            $products->whereBetween('price', [100, 500]);

        }
        if($request->price == 3){
            $products->whereBetween('price', [500, 1000]);
        }
        if($request->price == 4){
            $products->where('price', '>', 1000);
        }
        $search = $products->orderBy('id', 'DESC')->paginate(9);
        // $search = $products->get();
        // dd(($search));

        if(count($search) == 0){
            Session::flash('thongbao');
            return view('frontend.search.error_search', compact('search', 'brands', 'categories'))->render();

        }else{
            return view('frontend.search.list_search', compact('search', 'brands', 'categories'));
        }
    }
    // {
    //     $brands = Brand::all();
    //     $categories = Category::all();
    //     $products = Product::query();
    //     $search = $products
    //                     ->when($request->name)
    //                     ->where('name', 'like', "%$request->name%")
    //                     ->orderBy('id', 'DESC')->paginate(6);
    //     if(count($search) == 0){
    //         Session::flash('thongbao');
    //         return view('frontend.search.error_search', compact('search', 'brands', 'categories'))->render();

    //     }else{
    //         return view('frontend.search.list_search', compact('search', 'brands', 'categories'));
    //     }
    // }
    /**
     * Display a listing of the resource.
     * search with price range
     * @return \Illuminate\Http\Response
     */
    public function priceRange(Request $request)
    {
        $brands = Brand::all();
        $categories = Category::all();
        $min = (int)$request->min;
        // dd($min);
        $max = (int)$request->max;
        // dd($max);
        $search = Product::orderBy('id', 'ASC')->whereBetween('price', [$min, $max])->paginate(6);
        // dd($search);
        return view('frontend.search.list_search', compact('search', 'brands', 'categories'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
