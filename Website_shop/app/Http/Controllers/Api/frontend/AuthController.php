<?php

namespace App\Http\Controllers\Api\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class AuthController extends Controller
{
    public function showLogin ()
    {

        return response()->json([
            "success" => "login"
        ]);
    }
    public function checkLogin(LoginRequest $request)
    {
        $login = [
            'email' => $request->email,
            'password' => $request->password,
            'level' => 0
        ];
        $remember = false;
        if($request->remember_me)
        {
            $remember = true;
        }
        if(Auth::attempt($login, $remember))
        {

            // return redirect() -> intended(RouteServiceProvider::HOME_CLIENT);
            return response()->json([
                "success" => 'login thanh cong',
                "user" => Auth::user()
            ]);
            // return redirect()->route('home_frontend')->with('success', 'login ok');

        }else{
            return response()->json([
                "error" => 'login that bai'
            ]);
            // return redirect()->back()->withErrors('message','Email or password is not correct');
        }
    }
    public function showRegister()
    {
        return view ('frontend.auth.register');
    }
    public function postRegister(AuthRequest $request)
    {
        $data = $request->all();


        $members = new user();
        $members->name = $data['name'];
        $members->email = $data['email'];
        $members->phone = $data['phone'];
        $members->password = bcrypt($data['password']);
        $members->level = 0 ;

        // dd($members);
        $members->save();


        return redirect()->route('login');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
